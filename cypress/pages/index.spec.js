/// <reference types="cypress" />

describe('Home Page: Unauthenticated', () => {
  before(() => {
    cy.visit('/');
  });

  it('should redirect to login page', () => {
    cy.url().should('include', '/auth/login');
  });
});

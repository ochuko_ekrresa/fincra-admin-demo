/// <reference types="cypress" />

describe('Login Page: Unauthenticated', () => {
  beforeEach(() => {
    cy.visit('/auth/login');
  });

  it('should render the title', () => {
    cy.get('h1').should('have.text', 'Admin Login.');
  });
});

module.exports = {
  collectCoverageFrom: [
    '**/src/**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/node_modules/**',
  ],
  coverageReporters: ['html'],
  coverageThreshold: {
    global: {
      statements: 3.9,
      branches: 2.8,
      functions: 1.2,
      lines: 3.9,
    },
  },
  setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
  testPathIgnorePatterns: ['/node_modules/', '/.next/'],

  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest', // anything .js is babel'd for jest to consume
    '^.+.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': '<rootDir>/css-transform.js',
  },
  transformIgnorePatterns: ['/node_modules/', '^.+\\.module\\.(css|sass|scss)$'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less)$': 'identity-obj-proxy',
  },
};

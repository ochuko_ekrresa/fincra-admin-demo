const pusherConfig = {
  app_id: process.env.PUSHER_APP_ID,
  key: process.env.PUSHER_KEY,
  secret: process.env.PUSHER_SECRET,
  cluster: process.env.PUSHER_CLUSTER,
};

const env = {
  apiKey: process.env.API_KEY,
  backendURL: process.env.BACKEND_URL,
  hostname: process.env.HOSTNAME,
  pusher: pusherConfig,
};

module.exports = {
  env,
};

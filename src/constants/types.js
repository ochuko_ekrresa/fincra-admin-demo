export const TransactionTypes = Object.freeze({
  disbursement: 'disbursement',
  collection: 'collection',
  conversion: 'conversion',
});

export const SettlementTimes = Object.freeze({
  instant: 'instant',
  one_day: 'T+1',
  two_days: 'T+2',
  three_days: 'T+3',
});

export const SettlementDestinations = Object.freeze({
  bank_account: 'bank_account',
  fliqpay_wallet: 'fliqpay_wallet',
  crypto_wallet: 'crypto_wallet',
  mobile_money_wallet: 'mobile_money_wallet',
});

export const PaymentMethods = Object.freeze({
  card: 'card',
  bank_transfer: 'bank_transfer',
  crypto_transfer: 'crypto_transfer',
  mobile_money_transfer: 'mobile_money_transfer',
});

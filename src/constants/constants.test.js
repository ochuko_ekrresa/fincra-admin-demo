import countries from './countries';
import navigation from './navigation';

describe('Constants', () => {
  it('should not change unless snapshot changes', () => {
    expect(countries).toMatchInlineSnapshot(`
      Array [
        Object {
          "code": "AF",
          "name": "Afghanistan",
        },
        Object {
          "code": "AX",
          "name": "Åland Islands",
        },
        Object {
          "code": "AL",
          "name": "Albania",
        },
        Object {
          "code": "DZ",
          "name": "Algeria",
        },
        Object {
          "code": "AS",
          "name": "American Samoa",
        },
        Object {
          "code": "AD",
          "name": "AndorrA",
        },
        Object {
          "code": "AO",
          "name": "Angola",
        },
        Object {
          "code": "AI",
          "name": "Anguilla",
        },
        Object {
          "code": "AQ",
          "name": "Antarctica",
        },
        Object {
          "code": "AG",
          "name": "Antigua and Barbuda",
        },
        Object {
          "code": "AR",
          "name": "Argentina",
        },
        Object {
          "code": "AM",
          "name": "Armenia",
        },
        Object {
          "code": "AW",
          "name": "Aruba",
        },
        Object {
          "code": "AU",
          "name": "Australia",
        },
        Object {
          "code": "AT",
          "name": "Austria",
        },
        Object {
          "code": "AZ",
          "name": "Azerbaijan",
        },
        Object {
          "code": "BS",
          "name": "Bahamas",
        },
        Object {
          "code": "BH",
          "name": "Bahrain",
        },
        Object {
          "code": "BD",
          "name": "Bangladesh",
        },
        Object {
          "code": "BB",
          "name": "Barbados",
        },
        Object {
          "code": "BY",
          "name": "Belarus",
        },
        Object {
          "code": "BE",
          "name": "Belgium",
        },
        Object {
          "code": "BZ",
          "name": "Belize",
        },
        Object {
          "code": "BJ",
          "name": "Benin",
        },
        Object {
          "code": "BM",
          "name": "Bermuda",
        },
        Object {
          "code": "BT",
          "name": "Bhutan",
        },
        Object {
          "code": "BO",
          "name": "Bolivia",
        },
        Object {
          "code": "BA",
          "name": "Bosnia and Herzegovina",
        },
        Object {
          "code": "BW",
          "name": "Botswana",
        },
        Object {
          "code": "BV",
          "name": "Bouvet Island",
        },
        Object {
          "code": "BR",
          "name": "Brazil",
        },
        Object {
          "code": "IO",
          "name": "British Indian Ocean Territory",
        },
        Object {
          "code": "BN",
          "name": "Brunei Darussalam",
        },
        Object {
          "code": "BG",
          "name": "Bulgaria",
        },
        Object {
          "code": "BF",
          "name": "Burkina Faso",
        },
        Object {
          "code": "BI",
          "name": "Burundi",
        },
        Object {
          "code": "KH",
          "name": "Cambodia",
        },
        Object {
          "code": "CM",
          "name": "Cameroon",
        },
        Object {
          "code": "CA",
          "name": "Canada",
        },
        Object {
          "code": "CV",
          "name": "Cape Verde",
        },
        Object {
          "code": "KY",
          "name": "Cayman Islands",
        },
        Object {
          "code": "CF",
          "name": "Central African Republic",
        },
        Object {
          "code": "TD",
          "name": "Chad",
        },
        Object {
          "code": "CL",
          "name": "Chile",
        },
        Object {
          "code": "CN",
          "name": "China",
        },
        Object {
          "code": "CX",
          "name": "Christmas Island",
        },
        Object {
          "code": "CC",
          "name": "Cocos (Keeling) Islands",
        },
        Object {
          "code": "CO",
          "name": "Colombia",
        },
        Object {
          "code": "KM",
          "name": "Comoros",
        },
        Object {
          "code": "CG",
          "name": "Congo",
        },
        Object {
          "code": "CD",
          "name": "Congo, The Democratic Republic of the",
        },
        Object {
          "code": "CK",
          "name": "Cook Islands",
        },
        Object {
          "code": "CR",
          "name": "Costa Rica",
        },
        Object {
          "code": "CI",
          "name": "Cote D'Ivoire",
        },
        Object {
          "code": "HR",
          "name": "Croatia",
        },
        Object {
          "code": "CU",
          "name": "Cuba",
        },
        Object {
          "code": "CY",
          "name": "Cyprus",
        },
        Object {
          "code": "CZ",
          "name": "Czech Republic",
        },
        Object {
          "code": "DK",
          "name": "Denmark",
        },
        Object {
          "code": "DJ",
          "name": "Djibouti",
        },
        Object {
          "code": "DM",
          "name": "Dominica",
        },
        Object {
          "code": "DO",
          "name": "Dominican Republic",
        },
        Object {
          "code": "EC",
          "name": "Ecuador",
        },
        Object {
          "code": "EG",
          "name": "Egypt",
        },
        Object {
          "code": "SV",
          "name": "El Salvador",
        },
        Object {
          "code": "GQ",
          "name": "Equatorial Guinea",
        },
        Object {
          "code": "ER",
          "name": "Eritrea",
        },
        Object {
          "code": "EE",
          "name": "Estonia",
        },
        Object {
          "code": "ET",
          "name": "Ethiopia",
        },
        Object {
          "code": "FK",
          "name": "Falkland Islands (Malvinas)",
        },
        Object {
          "code": "FO",
          "name": "Faroe Islands",
        },
        Object {
          "code": "FJ",
          "name": "Fiji",
        },
        Object {
          "code": "FI",
          "name": "Finland",
        },
        Object {
          "code": "FR",
          "name": "France",
        },
        Object {
          "code": "GF",
          "name": "French Guiana",
        },
        Object {
          "code": "PF",
          "name": "French Polynesia",
        },
        Object {
          "code": "TF",
          "name": "French Southern Territories",
        },
        Object {
          "code": "GA",
          "name": "Gabon",
        },
        Object {
          "code": "GM",
          "name": "Gambia",
        },
        Object {
          "code": "GE",
          "name": "Georgia",
        },
        Object {
          "code": "DE",
          "name": "Germany",
        },
        Object {
          "code": "GH",
          "name": "Ghana",
        },
        Object {
          "code": "GI",
          "name": "Gibraltar",
        },
        Object {
          "code": "GR",
          "name": "Greece",
        },
        Object {
          "code": "GL",
          "name": "Greenland",
        },
        Object {
          "code": "GD",
          "name": "Grenada",
        },
        Object {
          "code": "GP",
          "name": "Guadeloupe",
        },
        Object {
          "code": "GU",
          "name": "Guam",
        },
        Object {
          "code": "GT",
          "name": "Guatemala",
        },
        Object {
          "code": "GG",
          "name": "Guernsey",
        },
        Object {
          "code": "GN",
          "name": "Guinea",
        },
        Object {
          "code": "GW",
          "name": "Guinea-Bissau",
        },
        Object {
          "code": "GY",
          "name": "Guyana",
        },
        Object {
          "code": "HT",
          "name": "Haiti",
        },
        Object {
          "code": "HM",
          "name": "Heard Island and Mcdonald Islands",
        },
        Object {
          "code": "VA",
          "name": "Holy See (Vatican City State)",
        },
        Object {
          "code": "HN",
          "name": "Honduras",
        },
        Object {
          "code": "HK",
          "name": "Hong Kong",
        },
        Object {
          "code": "HU",
          "name": "Hungary",
        },
        Object {
          "code": "IS",
          "name": "Iceland",
        },
        Object {
          "code": "IN",
          "name": "India",
        },
        Object {
          "code": "ID",
          "name": "Indonesia",
        },
        Object {
          "code": "IR",
          "name": "Iran, Islamic Republic Of",
        },
        Object {
          "code": "IQ",
          "name": "Iraq",
        },
        Object {
          "code": "IE",
          "name": "Ireland",
        },
        Object {
          "code": "IM",
          "name": "Isle of Man",
        },
        Object {
          "code": "IL",
          "name": "Israel",
        },
        Object {
          "code": "IT",
          "name": "Italy",
        },
        Object {
          "code": "JM",
          "name": "Jamaica",
        },
        Object {
          "code": "JP",
          "name": "Japan",
        },
        Object {
          "code": "JE",
          "name": "Jersey",
        },
        Object {
          "code": "JO",
          "name": "Jordan",
        },
        Object {
          "code": "KZ",
          "name": "Kazakhstan",
        },
        Object {
          "code": "KE",
          "name": "Kenya",
        },
        Object {
          "code": "KI",
          "name": "Kiribati",
        },
        Object {
          "code": "KP",
          "name": "Korea, Democratic People'S Republic of",
        },
        Object {
          "code": "KR",
          "name": "Korea, Republic of",
        },
        Object {
          "code": "KW",
          "name": "Kuwait",
        },
        Object {
          "code": "KG",
          "name": "Kyrgyzstan",
        },
        Object {
          "code": "LA",
          "name": "Lao People'S Democratic Republic",
        },
        Object {
          "code": "LV",
          "name": "Latvia",
        },
        Object {
          "code": "LB",
          "name": "Lebanon",
        },
        Object {
          "code": "LS",
          "name": "Lesotho",
        },
        Object {
          "code": "LR",
          "name": "Liberia",
        },
        Object {
          "code": "LY",
          "name": "Libyan Arab Jamahiriya",
        },
        Object {
          "code": "LI",
          "name": "Liechtenstein",
        },
        Object {
          "code": "LT",
          "name": "Lithuania",
        },
        Object {
          "code": "LU",
          "name": "Luxembourg",
        },
        Object {
          "code": "MO",
          "name": "Macao",
        },
        Object {
          "code": "MK",
          "name": "Macedonia, The Former Yugoslav Republic of",
        },
        Object {
          "code": "MG",
          "name": "Madagascar",
        },
        Object {
          "code": "MW",
          "name": "Malawi",
        },
        Object {
          "code": "MY",
          "name": "Malaysia",
        },
        Object {
          "code": "MV",
          "name": "Maldives",
        },
        Object {
          "code": "ML",
          "name": "Mali",
        },
        Object {
          "code": "MT",
          "name": "Malta",
        },
        Object {
          "code": "MH",
          "name": "Marshall Islands",
        },
        Object {
          "code": "MQ",
          "name": "Martinique",
        },
        Object {
          "code": "MR",
          "name": "Mauritania",
        },
        Object {
          "code": "MU",
          "name": "Mauritius",
        },
        Object {
          "code": "YT",
          "name": "Mayotte",
        },
        Object {
          "code": "MX",
          "name": "Mexico",
        },
        Object {
          "code": "FM",
          "name": "Micronesia, Federated States of",
        },
        Object {
          "code": "MD",
          "name": "Moldova, Republic of",
        },
        Object {
          "code": "MC",
          "name": "Monaco",
        },
        Object {
          "code": "MN",
          "name": "Mongolia",
        },
        Object {
          "code": "MS",
          "name": "Montserrat",
        },
        Object {
          "code": "MA",
          "name": "Morocco",
        },
        Object {
          "code": "MZ",
          "name": "Mozambique",
        },
        Object {
          "code": "MM",
          "name": "Myanmar",
        },
        Object {
          "code": "NA",
          "name": "Namibia",
        },
        Object {
          "code": "NR",
          "name": "Nauru",
        },
        Object {
          "code": "NP",
          "name": "Nepal",
        },
        Object {
          "code": "NL",
          "name": "Netherlands",
        },
        Object {
          "code": "AN",
          "name": "Netherlands Antilles",
        },
        Object {
          "code": "NC",
          "name": "New Caledonia",
        },
        Object {
          "code": "NZ",
          "name": "New Zealand",
        },
        Object {
          "code": "NI",
          "name": "Nicaragua",
        },
        Object {
          "code": "NE",
          "name": "Niger",
        },
        Object {
          "code": "NG",
          "name": "Nigeria",
        },
        Object {
          "code": "NU",
          "name": "Niue",
        },
        Object {
          "code": "NF",
          "name": "Norfolk Island",
        },
        Object {
          "code": "MP",
          "name": "Northern Mariana Islands",
        },
        Object {
          "code": "NO",
          "name": "Norway",
        },
        Object {
          "code": "OM",
          "name": "Oman",
        },
        Object {
          "code": "PK",
          "name": "Pakistan",
        },
        Object {
          "code": "PW",
          "name": "Palau",
        },
        Object {
          "code": "PS",
          "name": "Palestinian Territory, Occupied",
        },
        Object {
          "code": "PA",
          "name": "Panama",
        },
        Object {
          "code": "PG",
          "name": "Papua New Guinea",
        },
        Object {
          "code": "PY",
          "name": "Paraguay",
        },
        Object {
          "code": "PE",
          "name": "Peru",
        },
        Object {
          "code": "PH",
          "name": "Philippines",
        },
        Object {
          "code": "PN",
          "name": "Pitcairn",
        },
        Object {
          "code": "PL",
          "name": "Poland",
        },
        Object {
          "code": "PT",
          "name": "Portugal",
        },
        Object {
          "code": "PR",
          "name": "Puerto Rico",
        },
        Object {
          "code": "QA",
          "name": "Qatar",
        },
        Object {
          "code": "RE",
          "name": "Reunion",
        },
        Object {
          "code": "RO",
          "name": "Romania",
        },
        Object {
          "code": "RU",
          "name": "Russian Federation",
        },
        Object {
          "code": "RW",
          "name": "RWANDA",
        },
        Object {
          "code": "SH",
          "name": "Saint Helena",
        },
        Object {
          "code": "KN",
          "name": "Saint Kitts and Nevis",
        },
        Object {
          "code": "LC",
          "name": "Saint Lucia",
        },
        Object {
          "code": "PM",
          "name": "Saint Pierre and Miquelon",
        },
        Object {
          "code": "VC",
          "name": "Saint Vincent and the Grenadines",
        },
        Object {
          "code": "WS",
          "name": "Samoa",
        },
        Object {
          "code": "SM",
          "name": "San Marino",
        },
        Object {
          "code": "ST",
          "name": "Sao Tome and Principe",
        },
        Object {
          "code": "SA",
          "name": "Saudi Arabia",
        },
        Object {
          "code": "SN",
          "name": "Senegal",
        },
        Object {
          "code": "CS",
          "name": "Serbia and Montenegro",
        },
        Object {
          "code": "SC",
          "name": "Seychelles",
        },
        Object {
          "code": "SL",
          "name": "Sierra Leone",
        },
        Object {
          "code": "SG",
          "name": "Singapore",
        },
        Object {
          "code": "SK",
          "name": "Slovakia",
        },
        Object {
          "code": "SI",
          "name": "Slovenia",
        },
        Object {
          "code": "SB",
          "name": "Solomon Islands",
        },
        Object {
          "code": "SO",
          "name": "Somalia",
        },
        Object {
          "code": "ZA",
          "name": "South Africa",
        },
        Object {
          "code": "GS",
          "name": "South Georgia and the South Sandwich Islands",
        },
        Object {
          "code": "ES",
          "name": "Spain",
        },
        Object {
          "code": "LK",
          "name": "Sri Lanka",
        },
        Object {
          "code": "SD",
          "name": "Sudan",
        },
        Object {
          "code": "SR",
          "name": "Suriname",
        },
        Object {
          "code": "SJ",
          "name": "Svalbard and Jan Mayen",
        },
        Object {
          "code": "SZ",
          "name": "Swaziland",
        },
        Object {
          "code": "SE",
          "name": "Sweden",
        },
        Object {
          "code": "CH",
          "name": "Switzerland",
        },
        Object {
          "code": "SY",
          "name": "Syrian Arab Republic",
        },
        Object {
          "code": "TW",
          "name": "Taiwan, Province of China",
        },
        Object {
          "code": "TJ",
          "name": "Tajikistan",
        },
        Object {
          "code": "TZ",
          "name": "Tanzania, United Republic of",
        },
        Object {
          "code": "TH",
          "name": "Thailand",
        },
        Object {
          "code": "TL",
          "name": "Timor-Leste",
        },
        Object {
          "code": "TG",
          "name": "Togo",
        },
        Object {
          "code": "TK",
          "name": "Tokelau",
        },
        Object {
          "code": "TO",
          "name": "Tonga",
        },
        Object {
          "code": "TT",
          "name": "Trinidad and Tobago",
        },
        Object {
          "code": "TN",
          "name": "Tunisia",
        },
        Object {
          "code": "TR",
          "name": "Turkey",
        },
        Object {
          "code": "TM",
          "name": "Turkmenistan",
        },
        Object {
          "code": "TC",
          "name": "Turks and Caicos Islands",
        },
        Object {
          "code": "TV",
          "name": "Tuvalu",
        },
        Object {
          "code": "UG",
          "name": "Uganda",
        },
        Object {
          "code": "UA",
          "name": "Ukraine",
        },
        Object {
          "code": "AE",
          "name": "United Arab Emirates",
        },
        Object {
          "code": "GB",
          "name": "United Kingdom",
        },
        Object {
          "code": "US",
          "name": "United States",
        },
        Object {
          "code": "UM",
          "name": "United States Minor Outlying Islands",
        },
        Object {
          "code": "UY",
          "name": "Uruguay",
        },
        Object {
          "code": "UZ",
          "name": "Uzbekistan",
        },
        Object {
          "code": "VU",
          "name": "Vanuatu",
        },
        Object {
          "code": "VE",
          "name": "Venezuela",
        },
        Object {
          "code": "VN",
          "name": "Viet Nam",
        },
        Object {
          "code": "VG",
          "name": "Virgin Islands, British",
        },
        Object {
          "code": "VI",
          "name": "Virgin Islands, U.S.",
        },
        Object {
          "code": "WF",
          "name": "Wallis and Futuna",
        },
        Object {
          "code": "EH",
          "name": "Western Sahara",
        },
        Object {
          "code": "YE",
          "name": "Yemen",
        },
        Object {
          "code": "ZM",
          "name": "Zambia",
        },
        Object {
          "code": "ZW",
          "name": "Zimbabwe",
        },
      ]
    `);
    expect(navigation).toMatchInlineSnapshot(`
      Array [
        Object {
          "category": "",
          "links": Array [
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Overview",
              "route": "/overview",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Businesses",
              "route": "/businesses",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Merchants",
              "route": "/merchants",
            },
          ],
        },
        Object {
          "category": "Transactions",
          "links": Array [
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Collections",
              "route": "/collections",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Disbursements",
              "route": "/disbursements",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Order Book",
              "route": "/order-book",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Quotes",
              "route": "/quotes",
            },
          ],
        },
        Object {
          "category": "More",
          "links": Array [
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Corridor Mngt.",
              "route": "/corridor-management",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Exchange Rate Mngt.",
              "route": "/exchange-rate-management",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Settlement Confg.",
              "route": "/settlement-config",
            },
            Object {
              "icon": "/images/payment-link.svg",
              "name": "Settings",
              "route": "/settings",
            },
            Object {
              "icon": "/images/refund.svg",
              "name": "Sign out",
              "route": "/auth/logout",
            },
          ],
        },
      ]
    `);
  });
});

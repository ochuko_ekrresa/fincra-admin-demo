import * as ROUTES from './routes';

const NavigationLinks = [
  {
    category: 'Account Management',
    links: [
      {
        name: 'Wallet Top-Ups',
        icon: '/images/wallet.svg',
        activeIcon: '/images/wallet-active.svg',
        route: ROUTES.DASHBOARD.WALLET_TOP_UPS,
      },
      {
        name: 'Virtual Bank Accounts',
        activeIcon: '/images/virtual-accounts-active.svg',
        icon: '/images/virtual-accounts.svg',
        route: ROUTES.DASHBOARD.VIRTUAL_ACCOUNTS,
      },
      {
        name: 'Subaccounts',
        activeIcon: '/images/subaccounts-active.svg',
        icon: '/images/subaccounts.svg',
        route: ROUTES.DASHBOARD.SUB_ACCOUNTS,
      },
    ],
  },
  {
    category: 'Merchants',
    links: [
      {
        name: 'Merchant Requests',
        icon: '/images/merchants.svg',
        activeIcon: '/images/merchants-active.svg',
        route: ROUTES.DASHBOARD.MERCHANTS_REQUEST,
      },
      {
        name: 'Merchant Verification',
        icon: '/images/merchants.svg',
        activeIcon: '/images/merchants-active.svg',
        route: ROUTES.DASHBOARD.MERCHANTS_VERIFICATION,
      },
    ],
  },
  {
    category: 'Transactions',
    links: [
      {
        name: 'Conversion',
        icon: '/images/conversion.svg',
        activeIcon: '/images/conversion-active.svg',
        route: ROUTES.DASHBOARD.CONVERSIONS,
      },

      {
        name: 'Payouts',
        icon: '/images/payouts.svg',
        activeIcon: '/images/payouts-active.svg',
        route: ROUTES.DASHBOARD.PAYOUTS,
      },
    ],
  },
  {
    category: 'Configuration',
    links: [
      {
        name: 'Settings',
        icon: '/images/settings.svg',
        activeIcon: '/images/settings-active.svg',
        route: ROUTES.DASHBOARD.SETTINGS,
      },
      {
        name: 'Corridor Mgt',
        icon: '/images/corridor.svg',
        activeIcon: '/images/corridor-active.svg',
        route: ROUTES.DASHBOARD.CORRIDOR_MANAGEMENT,
      },
    ],
  },
];

export const SettingsNavigation = [
  { title: 'misc', url: ROUTES.DASHBOARD.SETTINGS },
  { title: 'volatility quotients', url: ROUTES.DASHBOARD.VOLATILITY_QUOTIENTS },
  // { title: "other", url: ROUTES.DASHBOARD.OTHERSETTINGS },
];

export default NavigationLinks;

export const LANDING = '/';
export const ERROR = '/error';
export const AUTH = {
  LOGIN: '/auth/login',
};
export const DASHBOARD = {
  HOME: '/overview',
  WALLET_TOP_UPS: '/wallet-top-ups',
  VIRTUAL_ACCOUNTS: '/virtual-accounts',
  SUB_ACCOUNTS: '/subaccounts',
  MERCHANTS_REQUEST: '/merchants-requests',
  MERCHANTS_VERIFICATION: '/merchants-verification',
  CONVERSIONS: '/conversion',
  PAYOUTS: '/payouts',
  CORRIDOR_MANAGEMENT: '/corridor-management',
  SETTINGS: '/settings',
};

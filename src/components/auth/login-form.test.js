import React from 'react';
import { axe } from 'jest-axe';
import user from '@testing-library/user-event';
import { render, getByTestId } from '@testing-library/react';
import LoginForm from './login-form';
import API from '../../utils/api';

jest.mock('../../utils/api');

describe('Login Form', () => {
  let container, loginButton, emailInput, passwordInput;

  beforeEach(() => {
    const { container: wrapper, getByLabelText } = render(<LoginForm />);
    container = wrapper;

    loginButton = getByTestId(container, 'login');
    emailInput = getByTestId(container, 'email');
    passwordInput = getByTestId(container, 'password');

    API.signIn.mockImplementation(() => {});

    getByLabelText(/email address/i);
    getByLabelText(/password/i);
  });

  test('the form is accessible', async () => {
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  test('the input is not read-only', () => {
    user.type(emailInput, 'root@email.com');
    user.type(passwordInput, 'rootPassword123');

    expect(emailInput.value).toBe('root@email.com');
    expect(passwordInput.value).toBe('rootPassword123');
  });

  it('should be disabled if form is empty', () => {
    expect(loginButton).toBeDisabled();
  });

  it("should be disabled if there's no email address", () => {
    user.type(passwordInput, 'testPassword098_');
    expect(loginButton).toBeDisabled();
  });

  it("should be disabled if there's no password", () => {
    user.type(emailInput, 'john@example.com');
    expect(loginButton).toBeDisabled();
  });

  it('should not be disabled if form is filled', () => {
    user.type(emailInput, 'john@example.com');
    user.type(passwordInput, 'testPassword098_');
    expect(loginButton).not.toBeDisabled();
  });

  it('should call onSubmit function', () => {
    user.type(emailInput, 'john@example.com');
    user.type(passwordInput, 'testPassword098_');
    user.click(loginButton);

    expect(API.signIn).toHaveBeenCalled();
  });

  // it("should match snapshot", () => {
  //   expect(container).toMatchInlineSnapshot();
  // });
});

import React, { useState, useEffect } from 'react';
import { signIn } from 'next-auth/client';
import { useRouter } from 'next/router';

import { Input, CheckBox } from '../Form';
import { Button } from '../';
import Toast from '../../utils/toast';
import { DASHBOARD } from '../../constants/routes';

export default function LoginForm() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [loginData, setLoginData] = useState({
    email: '',
    password: '',
    keepSignedIn: false,
    error: null,
  });

  useEffect(() => {
    if (loginData.error) {
      Toast({
        message: loginData.error,
      });
    }
  }, [loginData.error]);

  const handleSubmit = async evt => {
    evt.preventDefault();

    setLoading(true);

    const response = await signIn('credentials', {
      email: loginData.email,
      password: loginData.password,
      redirect: false,
    });

    setLoading(false);

    if (response.error) {
      return setLoginData({ ...loginData, error: response.error });
    }

    router.push(DASHBOARD.HOME);
  };

  const handleChange = evt => {
    setLoginData({ ...loginData, [evt.target.name]: evt.target.value });
  };

  return (
    <form data-testid="login-form" onSubmit={handleSubmit}>
      <Input
        name="email"
        title="Email Address"
        type="email"
        placeholder="Enter your email"
        wrapperClass="mb-6"
        value={loginData.email}
        onChange={handleChange}
        required={true}
      />
      <Input
        name="password"
        title="Password"
        type="password"
        placeholder="Enter your password"
        wrapperClass="mb-6"
        value={loginData.password}
        onChange={handleChange}
        required={true}
        isPassword={true}
      />
      <CheckBox
        name="keepSignedIn"
        value={loginData.keepSignedIn}
        onChange={evt =>
          setLoginData({
            ...loginData,
            [evt.target.name]: !loginData[evt.target.name],
          })
        }
        title="Keep me signed in"
        className="mb-6"
      />

      <Button
        data-testid="login"
        text="Log in"
        width="full"
        disabled={!(loginData.password && loginData.email)}
        loading={loading}
        className="mb-8"
      />

      <div className="px-4 py-5 font-medium bg-[#ece6fd] rounded">
        <h2 className="text-base text-secondary">Trouble signing in?</h2>
        <p>Talk to an admin if you have any issue.</p>
      </div>
    </form>
  );
}

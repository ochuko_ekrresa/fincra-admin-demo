import Card from './Card';
import { render } from '@testing-library/react';

describe('Card Component', () => {
  it('renders correctly', () => {
    const { container } = render(
      <Card title="Edit profile" subtitle="Use this form to edit your profile">
        <div>Hello children!</div>
      </Card>
    );

    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="p-6 rounded min-h-10 bg-white flex flex-col items-start w-full undefined"
        >
          <h2
            class="text-lg font-medium text-gray-800 mb-1"
          >
            Edit profile
          </h2>
          <span
            class="opacity-50 text-sm mb-6"
          >
            Use this form to edit your profile
          </span>
          <div>
            Hello children!
          </div>
        </div>
      </div>
    `);
  });
});

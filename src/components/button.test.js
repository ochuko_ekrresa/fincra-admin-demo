import Button from './Button';
import { render, getByTestId } from '@testing-library/react';
import user from '@testing-library/user-event';

describe('Button Component', () => {
  describe('renders correctly', () => {
    let buttonElement, container, rerender;

    beforeEach(() => {
      const { container: wrapper, rerender: newRerender } = render(
        <Button text="Click here" />
      );
      buttonElement = getByTestId(wrapper, 'button');
      container = wrapper;
      rerender = newRerender;
    });

    test('with text only', () => {
      expect(container).toMatchInlineSnapshot(`
        <div>
          <button
            class="uppercase bg-secondary hover:bg-blue-900 disabled:bg-blue-300 text-gray-100 outline-none focus:outline-none py-4 px-5 rounded inline-flex items-center justify-center text-sm font-medium undefined w-full md:w-56"
            data-testid="button"
          >
            <span>
              Click here
            </span>
          </button>
        </div>
      `);
    });

    test('with text and tooltip', () => {
      rerender(<Button text="Pending" tooltip="Click here to show pending stuffs" />);
      expect(container).toMatchInlineSnapshot(`
        <div>
          <button
            class="uppercase bg-secondary hover:bg-blue-900 disabled:bg-blue-300 text-gray-100 outline-none focus:outline-none py-4 px-5 rounded inline-flex items-center justify-center text-sm font-medium undefined w-full md:w-56"
            data-testid="button"
            title="Click here to show pending stuffs"
          >
            <span>
              Pending
            </span>
          </button>
        </div>
      `);
    });

    it('should be disabled when disabled or loading prop is passed', () => {
      rerender(<Button text="Click me" loading />);
      expect(buttonElement).toBeDisabled();

      rerender(<Button text="Click me" disabled />);
      expect(buttonElement).toBeDisabled();
    });

    test('with onClick', () => {
      const mockHandleClick = jest.fn();
      rerender(<Button text="Click me" onClick={mockHandleClick} />);

      user.click(buttonElement);

      expect(mockHandleClick).toBeCalled();
    });
  });
});

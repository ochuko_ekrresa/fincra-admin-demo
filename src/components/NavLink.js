import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';

export default function NavLink({ path, children, imgPath, activeImgPath }) {
  const { pathname } = useRouter();
  const isActive = pathname.startsWith(path);

  return (
    <div className={`flex hover:bg-white cursor-pointer ${isActive && 'bg-white'}`}>
      <div className="w-8">
        {isActive && <div className="w-1 bg-secondary h-full rounded-r-default"></div>}
      </div>
      <Link href={path}>
        <a className="flex items-start text-dark-100 hover:text-secondary py-3">
          <div className="flex mr-3">
            <Image
              src={isActive ? activeImgPath : imgPath}
              alt=""
              width={20}
              height={20}
              unoptimized
            />
          </div>

          <span className="capitalize">{children}</span>
        </a>
      </Link>
    </div>
  );
}

import Table from '../Table';

export default function HistoryTable({ data, onRowClickHandler }) {
  return (
    <Table
      columns={[
        { name: 'S/N', maxWidth: '4rem', minWidth: '1rem', selector: row => row.id },
        { name: 'Date Initiated', selector: row => row.date_initiated },
        {
          name: 'Merchant Name',
          selector: row => row.merchant_name,
        },
        { name: 'Currency', selector: row => row.currency },
        { name: 'Amount', selector: row => row.amount },
        {
          name: 'Status',
          cell: row => (
            <div
              className="bg-white-300 px-3 py-2 rounded-[4px] text-secondary whitespace-nowrap truncate"
              data-tag="allowRowEvents"
              title={row.status}
            >
              {row.status}
            </div>
          ),
        },
        {
          name: '',
          cell: () => (
            <button
              className="bg-white-300 px-3 py-2 rounded-[4px] text-secondary whitespace-nowrap hover:border hover:border-current/300"
              data-tag="allowRowEvents"
            >
              View
            </button>
          ),
        },
      ]}
      data={data}
      onRowClicked={onRowClickHandler}
      highlightOnHover
    />
  );
}

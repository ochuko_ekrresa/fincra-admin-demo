import { useState, useEffect } from 'react';
import {
  ListboxButton,
  ListboxList,
  ListboxInput,
  ListboxOption,
  ListboxPopover,
} from '@reach/listbox';
import { BiChevronLeft, BiChevronRight } from 'react-icons/bi';
import { FiChevronDown } from 'react-icons/fi';
import '@reach/listbox/styles.css';
import styled from 'styled-components';

import Table from '../Table';

export default function RequestTable({ data, onRowClickHandler, rowPerPageMonitor }) {
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(1);
  const [paginatedData, setPaginatedData] = useState([]);

  useEffect(() => {
    rowPerPageMonitor(rowsPerPage);
    const result = processData(data, page, rowsPerPage);
    setPaginatedData(result.paginatedData);
  }, [data, page, rowPerPageMonitor, rowsPerPage]);

  const handleRowsPerPage = val => {
    setRowsPerPage(Number(val));
  };

  const handlePageChange = val => {
    setPage(val);
  };

  return (
    <Table
      columns={[
        { name: 'S/N', maxWidth: '4rem', minWidth: '1rem', selector: row => row.id },
        { name: 'Date Initiated', selector: row => row.date_initiated },
        {
          name: 'Merchant Name',
          selector: row => row.merchant_name,
        },
        { name: 'Currency', selector: row => row.currency },
        { name: 'Amount', selector: row => row.amount },
        {
          name: 'Status',
          selector: row => row.status,
          cell: row => (
            <div
              className="bg-white-300 px-3 py-2 rounded-[4px] text-secondary whitespace-nowrap truncate"
              data-tag="allowRowEvents"
              title={row.status}
            >
              {row.status}
            </div>
          ),
        },
        {
          name: '',
          cell: () => (
            <button
              className="bg-white-300 px-3 py-2 rounded-[4px] text-secondary whitespace-nowrap hover:border hover:border-current"
              data-tag="allowRowEvents"
            >
              View
            </button>
          ),
        },
      ]}
      data={paginatedData}
      onRowClicked={onRowClickHandler}
      highlightOnHover
      onChangePage={handlePageChange}
      onChangeRowsPerPage={handleRowsPerPage}
      pagination
      paginationServer
      paginationComponent={CustomPagination}
    />
  );
}

const StyledListbox = styled.span`
  [data-reach-listbox-input] {
    margin: 0 0.5rem;
  }

  [data-reach-listbox-button] {
    border: 1px solid #877e9e;
    border-radius: 4px;
    padding: 0.3rem 0.2rem;
  }

  [data-reach-listbox-arrow] {
    margin-left: 0.1rem;
  }
`;

function CustomPagination({
  rowsPerPage,
  onChangePage,
  onChangeRowsPerPage,
  currentPage,
}) {
  const isFirstPage = currentPage === 1;

  return (
    <div className="flex items-center justify-between mt-12 py-2">
      <div className="flex items-center text-light text-sm">
        <span>Showing</span>
        <StyledListbox>
          <ListboxInput
            defaultValue="10"
            onChange={onChangeRowsPerPage}
            value={String(rowsPerPage)}
          >
            <ListboxButton arrow={<FiChevronDown className="text-base" />} />
            <ListboxPopover>
              <ListboxList>
                <ListboxOption value="5">5</ListboxOption>
                <ListboxOption value="10">10</ListboxOption>
                <ListboxOption value="25">25</ListboxOption>
                <ListboxOption value="30">30</ListboxOption>
              </ListboxList>
            </ListboxPopover>
          </ListboxInput>
        </StyledListbox>
        <span>entries per page</span>
      </div>

      <div className="flex items-center">
        <span
          className={`bg-fade-100 cursor-pointer text-[1.6rem] p-[0.1rem] rounded-sm mr-3 ${
            isFirstPage ? 'text-fade cursor-default' : ''
          }`}
          onClick={() => {
            isFirstPage ? null : onChangePage(currentPage - 1);
          }}
        >
          <BiChevronLeft />
        </span>

        <span className="bg-secondary cursor-pointer text-sm px-3 py-[0.3rem] rounded-sm mr-3 text-fade">
          {currentPage}
        </span>

        <span
          className="bg-fade-100 cursor-pointer text-[1.6rem] p-[0.1rem] rounded-sm mr-3"
          onClick={() => {
            onChangePage(currentPage + 1);
          }}
        >
          <BiChevronRight />
        </span>
      </div>
    </div>
  );
}

function processData(data = [], page = 1, itemsPerPage = 10) {
  const totalPages = Math.ceil(data.length / itemsPerPage);
  const startIndex = (page - 1) * itemsPerPage;

  const paginatedData = data.slice(startIndex).slice(0, itemsPerPage);

  return {
    currentPage: page,
    itemsPerPage,
    paginatedData,
    previousPage: page - 1 ? page - 1 : null,
    nextPage: totalPages > page ? page + 1 : null,
    totalItems: data.length,
    totalPages,
  };
}

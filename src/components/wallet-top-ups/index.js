export function DeclineApprovalModal({ cancelFn, confirmFn }) {
  return (
    <>
      <h2 className="text-dark font-semibold mb-2 text-2xl">Decline Request</h2>
      <p>Are you sure you want to decline this request?</p>
      <div className="flex mt-6">
        <button
          className="bg-red-700 text-white mr-4 px-5 py-1 rounded-[4px]"
          onClick={cancelFn}
        >
          No
        </button>
        <button
          className="bg-green-200 text-white px-5 py-1 rounded-[4px]"
          onClick={confirmFn}
        >
          Yes
        </button>
      </div>
    </>
  );
}

export function RequestApprovalModal({ cancelFn, confirmFn }) {
  return (
    <>
      <h2 className="text-dark font-semibold mb-2 text-2xl">Confirm Approval</h2>
      <p>Are you sure you want to approve this request?</p>
      <div className="flex mt-6">
        <button
          className="bg-red-700 text-white mr-4 px-5 py-1 rounded-[4px]"
          onClick={cancelFn}
        >
          No
        </button>
        <button
          className="bg-green-200 text-white px-5 py-1 rounded-[4px]"
          onClick={confirmFn}
        >
          Yes
        </button>
      </div>
    </>
  );
}

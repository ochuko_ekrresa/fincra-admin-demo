import { Button } from '../index';
import React from 'react';

export const NoDisbursements = () => (
  <div className="h-full w-full flex justify-center items-center my-8">
    <div className="md:w-2/3 h-xs bg-white rounded p-8 flex flex-col justify-around items-center">
      <img src="/images/transfer.svg" width="80" />
      <div className="text-center">
        <h2 className="text-gray-800 text-xl font-medium mb-1">Aw snap!</h2>
        <p className="opacity-50 text-sm">There's really nothing here for you to do.</p>
      </div>
      <a href="//www.youtube.com">
        <Button text="Watch videos" />
      </a>
    </div>
  </div>
);

export const Fetching = ({ title = 'data' }) => (
  <div className="h-full w-full flex justify-center items-center my-8">
    <div className="w-2/3 h-xs rounded p-8 flex flex-col justify-center items-center">
      <h2 className="text-center text-gray-600 text-base font-medium mb-1">
        Fetching {title}...
      </h2>
    </div>
  </div>
);

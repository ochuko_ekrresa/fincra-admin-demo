import React from 'react';

export default function CheckBox({
  name,
  type,
  checked,
  hideTitle,
  title,
  onChange,
  className,
  required,
}) {
  return (
    <div className={`CheckBox flex flex-col text-dark-100 text-sm ${className}`}>
      <label className="text-dark-100">
        <input
          data-testid={name}
          name={name}
          title={title}
          className="mr-3 leading-tight"
          onChange={onChange}
          type="checkbox"
          checked={checked}
          required={required}
        />
        {title && <span className={`text-sm ${hideTitle ? 'hidden' : ''}`}>{title}</span>}
        <span className="checkmark bg-current"></span>
      </label>
    </div>
  );
}

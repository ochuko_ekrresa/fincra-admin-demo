import CheckBox from './CheckBox';
import { render, getByTestId } from '@testing-library/react';
import user from '@testing-library/user-event';

describe('Checkbox Component', () => {
  let container;
  const mockOnChange = jest.fn();

  beforeEach(() => {
    const { container: wrapper, getByLabelText } = render(
      <CheckBox title="I agree to the terms." name="agree" onChange={mockOnChange} />
    );
    container = wrapper;
    getByLabelText(/I agree to the terms/i);
  });

  it('should not be checked by default', () => {
    const checkbox = getByTestId(container, 'agree');
    expect(checkbox).not.toBeChecked();
  });

  it('should call onChange when changed', () => {
    const checkbox = getByTestId(container, 'agree');
    user.click(checkbox);
    expect(checkbox).toBeChecked();
    expect(mockOnChange).toHaveBeenCalled();
  });

  it('should render correctly', () => {
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="CheckBox flex flex-col text-primarydark text-sm undefined"
        >
          <label
            class="text-primarydark"
          >
            <input
              class="mr-3 leading-tight"
              data-testid="agree"
              name="agree"
              title="I agree to the terms."
              type="checkbox"
            />
            <span
              class="text-sm "
            >
              I agree to the terms.
            </span>
            <span
              class="checkmark bg-current"
            />
          </label>
        </div>
      </div>
    `);
  });
});

import React, { useState } from 'react';
import Image from 'next/image';

const helperClasses = {
  info: 'text-gray-500',
  warning: 'text-yellow-500',
  danger: 'text-red-600',
  success: 'text-green-600',
  disabled: 'text-gray-200',
};

export default function Input({
  title,
  name,
  placeholder,
  type,
  wrapperClass,
  labelClass,
  inputClass,
  helper,
  prefix,
  isSearch,
  onChange,
  required,
  isPassword,
  ...otherInputProps
}) {
  const [showPassword, setShowPassword] = useState(true);
  const [inputType, setInputType] = useState(type);

  const borderStyle =
    helper && helper.type && helper.type !== 'info'
      ? { borderColor: 'currentColor' }
      : {};

  const showOrHidePassword = () => {
    setShowPassword(showPassword == true ? false : true);
    setInputType(showPassword ? 'input' : 'password');
  };

  return (
    <div
      className={`relative form-group flex flex-col text-sm ${wrapperClass} ${
        helper && helper.type && helper.type !== 'info'
          ? helperClasses[helper.type]
          : 'text-dark-100'
      }`}
    >
      {title && (
        <label htmlFor={name} className={`mb-1 font-medium ${labelClass}`}>
          {title}
        </label>
      )}
      <div className="relative inline-flex justify-start items-center">
        {prefix && (
          <span className="absolute flex items-center justify-center px-3 whitespace-no-wrap text-gray-500 text-sm w-12 h-12">
            {prefix}
          </span>
        )}
        <input
          data-testid={name}
          id={name}
          name={name}
          type={inputType}
          placeholder={placeholder}
          onChange={onChange}
          className={`text-base md:text-sm appearance-none border rounded px-3 py-2 outline-none w-full focus:bg-white ${inputClass} ${
            prefix && 'pl-10'
          }`}
          style={borderStyle}
          required={required}
          {...otherInputProps}
        />
        {isPassword && (
          <span
            className="absolute mr-3 right-0 top-2 cursor-pointer"
            onClick={showOrHidePassword}
          >
            <Image
              src={
                showPassword
                  ? '/images/password-visible.svg'
                  : '/images/password-hide-visibility.svg'
              }
              width={18}
              height={18}
              alt=""
              unoptimized
            />
          </span>
        )}
        {isSearch && (
          <span className="absolute px-3 right-0 top-2">
            <Image src="/images/search.svg" width={18} height={18} alt="" unoptimized />
          </span>
        )}
      </div>
      {helper && helper.message && (
        <p className={`${helperClasses[helper.type]} text-xs my-1 mx-3`}>
          {helper.message}
        </p>
      )}
    </div>
  );
}

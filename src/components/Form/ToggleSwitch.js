import React from 'react';

export default function ToggleSwitch({
  name,
  onChange,
  checked,
  required,
  disabled,
  ...otherProps
}) {
  return (
    <label htmlFor={name} className="ToggleSwitch">
      <input
        id={name}
        name={name}
        type="checkbox"
        onChange={onChange}
        checked={checked}
        required={required}
        disabled={disabled}
        {...otherProps}
      />
      <span className="slider round"></span>
    </label>
  );
}

import Input from './Input';
import { render, getByTestId } from '@testing-library/react';
import user from '@testing-library/user-event';

describe('Checkbox Component', () => {
  let container;
  const mockOnChange = jest.fn();

  beforeEach(() => {
    const { container: wrapper, getByLabelText } = render(
      <Input title="Your name" name="name" onChange={mockOnChange} />
    );
    container = wrapper;
    getByLabelText(/your name/i);
  });

  it('should be empty', () => {
    const input = getByTestId(container, 'name');
    expect(input).toBeEmpty();
  });

  it('should call onChange when changed', () => {
    const input = getByTestId(container, 'name');
    user.type(input, 'David Guetta');
    expect(mockOnChange).toHaveBeenCalled();
  });

  it('should render correctly', () => {
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="relative form-group flex flex-col text-sm undefined text-primarydark"
        >
          <label
            class="mb-1 font-medium undefined"
            for="name"
          >
            Your name
          </label>
          <div
            class="relative inline-flex justify-start items-center"
          >
            <input
              class="text-base md:text-sm appearance-none border rounded px-3 py-3 outline-none focus:bg-transparent w-full focus:border-primarydark focus:bg-white focus:border-dashed undefined  undefined"
              data-testid="name"
              id="name"
              name="name"
              value=""
            />
          </div>
        </div>
      </div>
    `);
  });
});

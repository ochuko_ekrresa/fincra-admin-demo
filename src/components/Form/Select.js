import React from 'react';
import ReactSelect from 'react-select';
import { capitalizeWords } from '../../helpers';

const helperClasses = {
  info: 'text-gray-500',
  warning: 'text-yellow-500',
  danger: 'text-red-600',
  success: 'text-teal-600',
  disabled: 'text-gray-300',
};

export function NewSelect({
  title,
  name,
  options: optionsArray = [],
  value: valueString = '',
  placeholder,
  wrapperClass,
  labelClass,
  selectClass,
  helperMessage,
  helperType,
  prefix,
  suffix,
  onChange,
  required,
  disabled,
  placeholderDisabled = true,
  ...otherSelectProps
}) {
  const options = optionsArray.map(option => {
    if (typeof option === 'string') {
      return { label: capitalizeWords(option, '_'), value: option };
    } else {
      return option;
    }
  });

  const value = Array.isArray(valueString)
    ? optionsArray.filter(option => valueString.includes(option.value))
    : optionsArray.find(option => option.value === valueString);

  const handleChange = (name, valueObject) => {
    let value;

    if (!valueObject) {
      value = '';
    } else {
      value = valueObject.label ? valueObject.value : valueObject.map(item => item.value);
    }

    onChange && onChange({ target: { name, value } });
  };

  return (
    <div className={`form-group flex flex-col text-dark-100 text-sm ${wrapperClass}`}>
      {title && (
        <label htmlFor={name} className={`mb-1 font-medium ${labelClass}`}>
          {title}
        </label>
      )}
      <div className="relative inline-flex justify-start items-center">
        {prefix && (
          <span className="absolute flex items-center justify-center px-3 whitespace-no-wrap text-gray-500 text-sm w-12 h-12">
            {prefix}
          </span>
        )}
        <ReactSelect
          data-testid={name}
          id={name}
          className={'w-full'}
          name={name}
          onChange={value => handleChange(name, value)}
          value={value}
          isMulti={Array.isArray(value)}
          required={required}
          disabled={disabled}
          options={options}
          styles={{
            control: provided => ({ ...provided, padding: '0.25rem' }),
          }}
          {...otherSelectProps}
        />
      </div>
      {helperMessage && (
        <p className={`${helperClasses[helperType]} text-xs m-1`}>{helperMessage}</p>
      )}
    </div>
  );
}

//
export default function Select({
  title,
  name,
  options,
  value = '',
  placeholder,
  wrapperClass,
  labelClass,
  selectClass,
  helperMessage,
  helperType,
  prefix,
  suffix,
  handleChange,
  required,
  disabled,
  placeholderDisabled = true,
  ...otherSelectProps
}) {
  return (
    <div className={`form-group flex flex-col text-dark-100 text-sm ${wrapperClass}`}>
      {title && (
        <label htmlFor={name} className={`mb-1 font-medium ${labelClass}`}>
          {title}
        </label>
      )}
      <div className="relative inline-flex justify-start items-center">
        {prefix && (
          <span className="absolute flex items-center justify-center px-3 whitespace-no-wrap text-gray-500 text-sm w-12 h-12">
            {prefix}
          </span>
        )}
        <select
          data-testid={name}
          id={name}
          className={`appearance-none border rounded px-3 py-3 outline-none w-full focus:border-dark-100 bg-white ${selectClass} ${
            !value ? 'text-gray-500' : 'text-dark-100 border-dark-100'
          } ${prefix && 'pl-12'}`}
          name={name}
          value={value}
          onChange={handleChange}
          required={required}
          disabled={disabled}
          {...otherSelectProps}
        >
          <option value="" label={placeholder} disabled={placeholderDisabled}>
            {placeholder}
          </option>
          {options &&
            options.length > 0 &&
            options.map(option => {
              const value = option.value ? option.value : option;
              let label = option.label ? option.label : option;

              if (typeof label === 'object') {
                label = 'undefined';
              }

              return (
                <option key={value} value={value} label={label}>
                  {label}
                </option>
              );
            })}
        </select>
      </div>
      {helperMessage && (
        <p className={`${helperClasses[helperType]} text-xs m-1`}>{helperMessage}</p>
      )}
    </div>
  );
}

import React from 'react';

export default function RadioGroup({
  className,
  name,
  options,
  onChange,
  orientation,
  selectedOption,
  required,
}) {
  return (
    <div
      className={`flex justify-between RadioGroup ${className} ${
        orientation === 'vertical' ? 'flex-col' : ''
      }`}
    >
      {options.length > 0 &&
        options.map(({ value, title }, idx) => (
          <div
            key={idx}
            className={'Radio flex flex-col text-dark-100 text-sm mr-2 w-full'}
          >
            <label htmlFor={`${name}-${title}-${idx}`} className="text-dark-100">
              <input
                data-testid={name}
                id={`${name}-${title}-${idx}`}
                name={name}
                className="mr-2 leading-tight"
                onChange={onChange}
                type="radio"
                value={value}
                checked={selectedOption === value}
                required={required}
              />
              <span className="text-sm text-dark-100">{title}</span>
              <span className="checkmark bg-current"></span>
            </label>
          </div>
        ))}
    </div>
  );
}

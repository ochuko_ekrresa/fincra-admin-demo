import { PageLoader } from './PageLoader';
import { ComponentLoader } from './ComponentLoader';

export { ComponentLoader, PageLoader };

import { render } from '@testing-library/react';
import PageLoader from './PageLoader';

describe('Page Loader component', () => {
  it('should render correctly', () => {
    const { container } = render(<PageLoader />);
    expect(container).toMatchInlineSnapshot('<div />');
  });
});

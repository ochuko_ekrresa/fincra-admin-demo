import { render } from '@testing-library/react';
import ComponentLoader from './ComponentLoader';

describe('Component Loader component', () => {
  it('should render correctly', () => {
    const { container } = render(<ComponentLoader />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div />
      </div>
    `);
  });
});

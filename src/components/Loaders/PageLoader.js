export default function PageLoader() {
  return (
    <div className="absolute inset-0 z-40 h-full bg-white flex justify-center items-center">
      <img
        src="/images/fliqpay-loader.gif"
        className="w-12 opacity-75"
        alt="Loading..."
      />
    </div>
  );
}

import SelectAcceptedCurrencies from './SelectAcceptedCurrencies';
import { render, getAllByTestId } from '@testing-library/react';
import user from '@testing-library/user-event';
import API from '../utils/api';

jest.mock('../utils/api');

API.getAdminCurrencies = jest.fn().mockImplementation(success =>
  success([
    {
      name: 'Bitcoin',
      code: 'BTC',
      currencyType: 'crypto',
    },
    {
      name: 'Litecoin',
      code: 'LTC',
      currencyType: 'crypto',
    },
  ])
);

const mockSetAcceptedCurrencies = jest.fn();

describe('Select Accepted Currencies Component', () => {
  let container, rerender;

  beforeEach(() => {
    const { container: wrapper, rerender: newRerender } = render(
      <SelectAcceptedCurrencies
        acceptedCurrencies={[]}
        setAcceptedCurrencies={mockSetAcceptedCurrencies}
      />
    );
    container = wrapper;
    rerender = newRerender;
  });

  it('should render correctly', () => {
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="flex flex-col text-sm w-full -mt-2"
        >
          <div
            class="grid "
            style="display: grid; grid-template-columns: repeat(5, 1fr); gap: .5rem;"
          >
            <div
              class="border-blue-100 hover:border-blue-200 border border-transparent text-sm rounded bg-white flex items-center justify-between cursor-pointer "
              data-testid="currency-card"
            >
              <div
                class="flex m-2"
              >
                <img
                  src="/images/btc-square.svg"
                />
                <span
                  class="-mb-1 ml-2 text-uppercase"
                >
                  BTC
                </span>
              </div>
            </div>
            <div
              class="border-blue-100 hover:border-blue-200 border border-transparent text-sm rounded bg-white flex items-center justify-between cursor-pointer "
              data-testid="currency-card"
            >
              <div
                class="flex m-2"
              >
                <img
                  src="/images/ltc-square.svg"
                />
                <span
                  class="-mb-1 ml-2 text-uppercase"
                >
                  LTC
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
  });

  it('should select accepted currencies', () => {
    const cards = getAllByTestId(container, 'currency-card');
    cards.forEach(card => {
      user.click(card);
    });
    expect(mockSetAcceptedCurrencies).toHaveBeenCalled();
  });

  it('should unselect accepted currencies', () => {
    const cards = getAllByTestId(container, 'currency-card');
    cards.forEach(card => {
      user.click(card);
    });
    expect(mockSetAcceptedCurrencies).toHaveBeenCalled();
  });
});

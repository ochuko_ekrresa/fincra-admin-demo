import React from 'react';

const variants = {
  primary: 'bg-purple-800 hover:bg-purple-900 disabled:bg-purple-300',
  secondary: 'bg-secondary hover:bg-blue-900 disabled:bg-[#c7b4f9]',
  info: 'bg-gray-300 text-gray-900 hover:bg-gray-300 disabled:bg-gray-300 disabled:text-gray-100',
  danger: 'bg-red-600 hover:bg-red-800 disabled:bg-red-300',
  warning: 'bg-yellow-600 hover:bg-yellow-700 disabled:bg-orange-300',
  success: 'bg-green-600 hover:bg-green-800 disabled:bg-green-300',
  black: 'bg-gray-900 hover:bg-black disabled:bg-gray-300',
};

export default function Button({
  className,
  disabled,
  loading,
  icon,
  type = 'secondary',
  buttonType,
  text,
  tooltip,
  onClick,
  padding = 4,
  width = '56',
  ...otherButtonProps
}) {
  return (
    <button
      data-testid="button"
      className={`uppercase ${
        variants[type]
      } text-gray-100 outline-none focus:outline-none py-${padding} px-${
        padding + 1
      } rounded inline-flex items-center justify-center text-sm font-medium ${className} w-full md:w-${width}`}
      title={tooltip}
      disabled={disabled || loading}
      onClick={onClick}
      type={buttonType}
      {...otherButtonProps}
    >
      {icon}
      <span>{text}</span>
      {loading && (
        <img src="/images/loading.gif" alt="loading" className="w-5 ml-2 -mt-1" />
      )}
    </button>
  );
}

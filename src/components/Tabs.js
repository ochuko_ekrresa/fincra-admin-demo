import React, { useState } from 'react';

const activeClassName =
  'border-b-4 border-secondary font-medium text-secondary rounded-t';

export default function Tabs({ components }) {
  const navItems = Array.from(Object.keys(components));
  const [activeItem, setActiveItem] = useState(0);

  return (
    components && (
      <div className="w-full">
        <div data-testid="tabs-menu" className="inline-flex">
          <ul className="flex h-10 items-center justify-center text-sm my-4">
            {navItems?.length > 0 &&
              navItems.map((navItem, idx) => (
                <li key={idx} className="h-full text-gray-400">
                  <button
                    className={`mr-6 h-full flex items-center font-normal no-underline p-2 px-6 capitalize ${
                      activeItem === idx ? activeClassName : ''
                    }`}
                    onClick={() => setActiveItem(idx)}
                  >
                    {navItem}
                  </button>
                </li>
              ))}
          </ul>
        </div>
        <div data-testid="active-tab" className="">
          {components[navItems[activeItem]]}
        </div>
      </div>
    )
  );
}

import React from 'react';

const variants = {
  primary: 'text-purple-800 border-purple-800 bg-purple-100 disabled:text-purple-300',
  secondary: 'text-blue-800 border-blue-800 bg-blue-100 disabled:text-blue-300',
  info: 'text-gray-600 border-gray-600 bg-gray-100 disabled:text-gray-300',
  danger: 'text-red-600 border-red-600 bg-red-100 disabled:text-red-300',
  warning: 'text-yellow-600 border-yellow-600 bg-yellow-100 disabled:text-orange-300',
  success: 'text-green-600 border-green-600 bg-green-100 disabled:text-green-300',
  black: 'text-gray-900 border-gray-900 bg-gray-100 disabled:text-gray-300',
};

const dotVariants = {
  primary: 'bg-purple-800',
  secondary: 'bg-blue-800',
  info: 'bg-gray-600',
  danger: 'bg-red-600',
  warning: 'bg-yellow-600',
  success: 'bg-green-600',
  black: 'bg-gray-900',
};

export default function Badge({
  className,
  dot,
  type,
  title,
  tooltip,
  tooltipPosition = 'top',
}) {
  return !dot ? (
    <span
      className={`${className} ${variants[type]} inline-flex py-1 px-4 uppercase border rounded text-xs font-semibold cursor-pointer`}
      uk-tooltip={`${tooltip ? `title:${tooltip}; pos: ${tooltipPosition}` : ''}`}
    >
      <span className="pt-px">{title}</span>
    </span>
  ) : (
    <span
      className={`inline-flex p-2 rounded ${dotVariants[type]} capitalize`}
      uk-tooltip={`${`title:${tooltip || title}; pos: ${tooltipPosition}`}`}
    ></span>
  );
}

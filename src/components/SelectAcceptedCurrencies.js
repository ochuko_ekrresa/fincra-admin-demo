import React, { useState, useEffect } from 'react';
import { Grid } from './Layout';
import API from '../utils/api';

const CurrencyCard = ({ currency, checked, handleClick, ...otherProps }) => {
  const onClick = () => {
    handleClick(currency.code);
  };

  return (
    <div
      {...otherProps}
      className={`${
        checked
          ? 'border-dark-100 hover:border-dark-100'
          : 'border-blue-100 hover:border-blue-200'
      } border border-transparent text-sm rounded bg-white flex items-center justify-between cursor-pointer `}
      onClick={onClick}
    >
      <div className="flex m-2">
        <img src={`/images/${currency.code.toLowerCase()}-square.svg`} />
        <span className="-mb-1 ml-2 text-uppercase">{currency.code}</span>
      </div>
    </div>
  );
};

const SelectAcceptedCurrencies = ({ acceptedCurrencies, setAcceptedCurrencies }) => {
  const [availableCurrencies, setAvailableCurrencies] = useState([]);

  const toggleCurrencySelection = currency => {
    let newAcceptedCurrencies = [...acceptedCurrencies];

    if (newAcceptedCurrencies.includes(currency)) {
      newAcceptedCurrencies = newAcceptedCurrencies.filter(
        acceptedCurrency => acceptedCurrency !== currency
      );
    } else {
      newAcceptedCurrencies.push(currency);
    }

    setAcceptedCurrencies(newAcceptedCurrencies);
  };

  useEffect(() => {
    API.getAdminCurrencies(currencies => setAvailableCurrencies(currencies));
  }, []);

  return (
    <div className="flex flex-col text-sm w-full -mt-2">
      <Grid columns={5} gap={'.5rem'} className="">
        {availableCurrencies?.map((currency, idx) => (
          <CurrencyCard
            data-testid="currency-card"
            key={idx}
            currency={currency}
            handleClick={toggleCurrencySelection}
            checked={acceptedCurrencies?.includes(currency.code)}
          />
        ))}
      </Grid>
    </div>
  );
};

export default SelectAcceptedCurrencies;

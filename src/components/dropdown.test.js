import Dropdown from './Dropdown';
import { render, getByText, queryByTestId, getByTestId } from '@testing-library/react';
import user from '@testing-library/user-event';

describe('Dropdown Component', () => {
  let container, rerender;
  const placeholder = 'More options';
  const options = ['Option 1', 'Option 2', 'Option 3'];

  const mockOnClick = jest.fn();
  const mockOnSelect = jest.fn();

  beforeEach(() => {
    const { container: wrapper, rerender: newRerender } = render(
      <Dropdown
        placeholder={placeholder}
        options={options}
        onClick={mockOnClick}
        onSelect={mockOnSelect}
      />
    );
    container = wrapper;
    rerender = newRerender;
  });

  it('should render button text', () => {
    expect(getByText(container, placeholder)).toMatchInlineSnapshot(`
      <span
        class="mr-1"
      >
        More options
      </span>
    `);
  });

  it('should not be open by default', () => {
    expect(queryByTestId(container, 'dropdown-items')).toBe(null);
  });

  it('should be open when clicked', () => {
    const dropdownButton = getByTestId(container, 'dropdown-button');

    user.click(dropdownButton);
    expect(queryByTestId(container, 'dropdown-items')).toMatchInlineSnapshot(`
      <ul
        class="absolute right-0 text-gray-700 pt-1 block w-56 shadow-sm rounded overflow-hidden"
        data-testid="dropdown-items"
      >
        <li>
          <button
            class="rounded-t bg-white hover:bg-gray-100 py-3 px-4 block text-center whitespace-no-wrap w-full text-sm"
          >
            Option 1
          </button>
        </li>
        <li>
          <button
            class="rounded-t bg-white hover:bg-gray-100 py-3 px-4 block text-center whitespace-no-wrap w-full text-sm"
          >
            Option 2
          </button>
        </li>
        <li>
          <button
            class="rounded-t bg-white hover:bg-gray-100 py-3 px-4 block text-center whitespace-no-wrap w-full text-sm"
          >
            Option 3
          </button>
        </li>
      </ul>
    `);
    expect(mockOnClick).toHaveBeenCalled();
  });

  test('onSelect returns the selected value', () => {
    const dropdownButton = getByTestId(container, 'dropdown-button');

    options.forEach(option => {
      // open dropdown each time because the dropdown closes after onSelect is called
      user.click(dropdownButton);
      user.click(getByText(container, option));
    });
    expect(mockOnSelect).toHaveBeenCalledTimes(options.length);
  });
});

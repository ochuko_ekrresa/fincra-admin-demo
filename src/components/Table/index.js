import DataTable, { createTheme } from 'react-data-table-component';

createTheme('default', {
  text: {
    primary: '#1F026B',
    secondary: '#4305EB',
  },
  background: {
    default: '#fff',
  },
  highlightOnHover: {
    default: '#f7f7fc',
  },
  divider: {
    default: '#f2f2f2',
  },
});

const defaultStyles = {
  headRow: {
    style: {
      minHeight: '56px',
      backgroundColor: ' #F7F7FC',
      borderRadius: '4px',
    },
  },
  headCells: {
    style: {
      color: '#000000',
      fontSize: '14px',
      fontWeight: 500,
    },
  },
};

export default function Table({
  data,
  columns,
  customStyles = defaultStyles,
  theme = 'default',
  ...props
}) {
  return (
    <DataTable
      theme={theme}
      columns={columns}
      data={data}
      customStyles={customStyles}
      noHeader
      pointerOnHover
      responsive
      {...props}
    />
  );
}

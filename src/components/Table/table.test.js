import { render } from '@testing-library/react';
import Table from '.';

describe('Table Component', () => {
  it('should render correctly', () => {
    // Snapshot testing fails because table classes are dynamic
    const { container } = render(
      <Table
        columns={[
          {
            name: 'Amount',
            selector: 'totalAmount',
            sortable: true,
          },
        ]}
        data={[{ totalAmount: 200 }]}
      />
    );
    expect(container).toHaveTextContent('200');
  });
});

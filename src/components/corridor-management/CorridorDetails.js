import { Grid } from '../Layout';
import Link from 'next/link';
import { Button } from '../index';
import React from 'react';

export const CorridorDataComponent = ({ corridor }) => (
  <div className="w-full rounded grid grid-cols-2">
    {corridor.config.map((config, idx) => (
      <div key={idx} className="p-3 h-auto m-2 my-3 bg-white px-4">
        <h4 className="text-base text-gray-700 font-medium mb-1">Config</h4>
        <div className="grid grid-cols-2 gap-2">
          {Object.keys(config).map(
            name =>
              typeof config[name] !== 'object' && (
                <div className="flex flex-col mb-2">
                  <span className="text-sm text-gray-600 mb-px">{name}</span>
                  <span className="text-sm "> {config[name]}</span>
                </div>
              )
          )}
        </div>
        <div className="my-2">
          <h4 className="text-base text-gray-700 font-medium mb-2">Channels</h4>
          <div className="grid grid-cols-2 gap-1 text-sm">
            <p className="text-gray-700">Name</p>
            <p className="text-gray-700">Type</p>
            {config?.channels?.map(channel => (
              <>
                <div>{channel.name}</div>
                <div>{channel.type}</div>
              </>
            ))}
          </div>
        </div>
      </div>
    ))}
  </div>
);

const CorridorDetails = ({ data: corridor }) => (
  <Grid columns="1" gap="1.5rem" className="w-full text-dark-100 -mx-2">
    <div className="">
      <Link href={`/corridor-management/edit/${corridor._id}`}>
        <Button buttonType="button" type="info" text="Modify" padding="2" width="32" />
      </Link>
      <CorridorDataComponent corridor={corridor} />
    </div>
  </Grid>
);

export default CorridorDetails;

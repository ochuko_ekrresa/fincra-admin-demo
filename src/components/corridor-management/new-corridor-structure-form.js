import React, { useState, useEffect } from 'react';
import { Formik, Form, Field, FieldArray } from 'formik';

import Button from '../Button';
import { Grid } from '../Layout';
import { Input, Select } from '../Form';

import API from '../../utils/api';
import localStorage from '../../utils/localstorage';
import Toast from '../../utils/toast';
import TextButton from '../TextButton';

const initialState = {
  sourceCurrency: '',
  destinationCurrency: '',
  transactionType: '',
  config: [],
};

export default function NewCorridorStructureForm({ onCreate, closeModal }) {
  const [loading, setLoading] = useState(false);
  const [currencies, setCurrencies] = useState(null);
  const [channels, setChannels] = useState(null);
  const [transactionTypes, setTransactionTypes] = useState(null);

  useEffect(() => {
    API.getCurrencies(currencies => setCurrencies(currencies));
    API.getChannels(channels => setChannels(channels));
    API.getTransactionTypes(transactionTypes => setTransactionTypes(transactionTypes));
  }, []);

  const onSubmit = async (data, { resetForm }) => {
    const { sourceCurrency, destinationCurrency, transactionType, config } = data;

    data.config.forEach((config, idx) => {
      const channels = data.config[idx].channels;
      data.config[idx].channels = channels.map(channel => JSON.parse(channel));
    });

    if (sourceCurrency && destinationCurrency && transactionType && config.length > 0) {
      setLoading(true);
      await API.createCorridorStructure(
        data,
        data => {
          Toast({
            title: 'Success',
            type: 'success',
            message: data.message,
          });
          // onCreate(data);
          resetForm(initialState);
          setLoading(false);
          // closeModal();
        },
        error => {
          setLoading(false);
          Toast({ title: 'Error', type: 'danger', message: error.error });
        }
      );
    }
  };

  return (
    // <form className="mt-3 mb-2" onSubmit={onSubmit}>
    <Formik initialValues={initialState} onSubmit={onSubmit}>
      {({ values, getFieldProps }) => (
        <Form className="w-full">
          <div className="mt-2">
            <div className="flex items-start">
              <Grid columns={1} gap={'1.3rem'} className="w-1/3 mr-8">
                <Select
                  name="sourceCurrency"
                  title="Source Currency"
                  options={currencies?.sourceCurrencies.map(({ name, code }) => ({
                    label: name,
                    value: code,
                  }))}
                  {...getFieldProps('sourceCurrency')}
                />

                <Select
                  name="destinationCurrency"
                  title="Destination Currency"
                  options={currencies?.destinationCurrencies.map(({ name, code }) => ({
                    label: name,
                    value: code,
                  }))}
                  {...getFieldProps('destinationCurrency')}
                />

                <Select
                  name="transactionType"
                  title="Transaction Type"
                  options={transactionTypes}
                  {...getFieldProps('transactionType')}
                />

                <Button
                  buttonType="submit"
                  data-testid="create"
                  text="Create corridor"
                  type="success"
                  width={'full'}
                  loading={loading}
                  disabled={
                    !(
                      values.sourceCurrency &&
                      values.destinationCurrency &&
                      values.transactionType &&
                      values.config.length
                    )
                  }
                />
              </Grid>
              <div className="w-2/3 h-xl overflow-y-auto">
                <FieldArray
                  name="config"
                  render={arrayHelpers => (
                    <>
                      {values.config && values.config.length > 0 ? (
                        <>
                          {values.config.map((config, index) => (
                            <React.Fragment key={index}>
                              <p className="mb-1 text-sm font-medium undefined">
                                Config {index + 1}
                              </p>

                              <div className="ml-4 my-2 pb-4 border-b border-gray">
                                <Grid columns={1} gap={'1.3rem'} key={index}>
                                  <Select
                                    name={`config.${index}.sourceCurrency`}
                                    title="Source Currency"
                                    required
                                    options={currencies?.sourceCurrencies.map(
                                      ({ name, code }) => ({
                                        label: name,
                                        value: code,
                                      })
                                    )}
                                    {...getFieldProps(`config.${index}.sourceCurrency`)}
                                  />

                                  <Select
                                    name={`config.${index}.destinationCurrency`}
                                    title="Destination Currency"
                                    required
                                    options={currencies?.destinationCurrencies.map(
                                      ({ name, code }) => ({
                                        label: name,
                                        value: code,
                                      })
                                    )}
                                    {...getFieldProps(
                                      `config.${index}.destinationCurrency`
                                    )}
                                  />

                                  <Select
                                    name={`config.${index}.action`}
                                    title="Action"
                                    required
                                    options={transactionTypes}
                                    {...getFieldProps(`config.${index}.action`)}
                                  />

                                  <p className="mb-1 text-sm font-medium undefined">
                                    Channels
                                  </p>
                                  <FieldArray
                                    name={`config.${index}.channels`}
                                    render={arrayHelpers => (
                                      <>
                                        {values.config[index].channels &&
                                        values.config[index].channels.length > 0 ? (
                                          <div className="pl-4">
                                            {values.config[index].channels.map(
                                              (channel, channelIndex) => (
                                                <Grid
                                                  columns={2}
                                                  className="my-2"
                                                  gap={'1.3rem'}
                                                  key={channelIndex}
                                                >
                                                  <Select
                                                    name={`config.${index}.channels.${channelIndex}`}
                                                    required
                                                    placeholder="Choose corridor"
                                                    options={channels?.map(
                                                      ({ name, type }) => ({
                                                        label: `${name} (${type})`,
                                                        value: JSON.stringify({
                                                          name,
                                                          type,
                                                        }),
                                                      })
                                                    )}
                                                    {...getFieldProps(
                                                      `config.${index}.channels.${channelIndex}`
                                                    )}
                                                  />
                                                </Grid>
                                              )
                                            )}
                                            <div className="flex mt-2">
                                              <Button
                                                buttonType="button"
                                                className="mr-2"
                                                type="danger"
                                                text="-"
                                                padding="2"
                                                width="12"
                                                onClick={() => arrayHelpers.remove(index)}
                                              />
                                              <Button
                                                buttonType="button"
                                                type="success"
                                                text="+"
                                                padding="2"
                                                width="12"
                                                onClick={() => arrayHelpers.push('')}
                                              />
                                            </div>
                                          </div>
                                        ) : (
                                          <Button
                                            buttonType="button"
                                            type="info"
                                            className="-mt-4"
                                            text="Add channel"
                                            padding="2"
                                            width={32}
                                            onClick={() => arrayHelpers.push('')}
                                          />
                                        )}
                                      </>
                                    )}
                                  />
                                </Grid>
                                <div className="flex mt-4">
                                  <TextButton
                                    buttonType="button"
                                    className="mr-2"
                                    type="danger"
                                    text="Delete config"
                                    padding="2"
                                    onClick={() => arrayHelpers.remove(index)}
                                  />
                                  <Button
                                    buttonType="button"
                                    type="info"
                                    text="New config"
                                    padding="2"
                                    width="32"
                                    onClick={() =>
                                      arrayHelpers.push({
                                        sourceCurrency: '',
                                        destinationCurrency: '',
                                        action: '',
                                        channels: [],
                                      })
                                    }
                                  />
                                </div>
                              </div>
                            </React.Fragment>
                          ))}
                        </>
                      ) : (
                        <>
                          <p className="mb-1 text-sm font-medium undefined">Configs</p>

                          <Button
                            buttonType="button"
                            type="info"
                            text="Add config +"
                            padding="2"
                            width="32"
                            onClick={() => arrayHelpers.push({})}
                          />
                        </>
                      )}
                    </>
                  )}
                />
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
    // </form>
  );
}

import React, { useState } from 'react';

import Button from '../Button';
import Modal from '../Modal';

import NewCorridorStructureForm from './new-corridor-structure-form';

export default function NewCorridorStructure({ onCreate }) {
  const [isModalOpen, setModalOpen] = useState(false);

  const openModal = () => {
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  return (
    <>
      <Button type="secondary" text="New Corridor" onClick={openModal} />
      <Modal
        size="4xl"
        isOpen={isModalOpen}
        title={'Create Corridor Structure'}
        subtitle="Create a new corridor structure."
        onRequestClose={closeModal}
      >
        <NewCorridorStructureForm onCreate={onCreate} closeModal={closeModal} />
      </Modal>
    </>
  );
}

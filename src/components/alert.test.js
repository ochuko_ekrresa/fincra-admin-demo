import Alert from './Alert';
import { render } from '@testing-library/react';

describe('Alert Component', () => {
  it('should render correctly', () => {
    const { container } = render(
      <Alert title="Hi there!" message="We noticed that you blah..." canClose />
    );
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="flex text-left relative bg-gray-100 border-t- border-gray-500 rounded-b text-gray-900 px-4 py-3 md:m-4 w-screen md:w-undefined undefined"
          role="alert"
        >
          <div
            class=""
          >
            <span
              class="absolute top-0 bottom-0 right-0 px-4 py-3"
            >
              <svg
                class="fill-current h-6 w-6"
                role="button"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>
                  Close
                </title>
                <path
                  d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"
                />
              </svg>
            </span>
            <div>
              <p
                class="font-semibold"
              >
                Hi there!
              </p>
              <p
                class="text-sm"
              >
                We noticed that you blah...
              </p>
            </div>
          </div>
        </div>
      </div>
    `);
  });
});

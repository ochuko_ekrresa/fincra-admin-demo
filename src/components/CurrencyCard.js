import React, { useEffect, useState } from 'react';
import API from '../utils/api';
import { getCurrencyAndName } from '../helpers';

export default function CurrencyCard({ currency, className, iconOnly }) {
  const [adminCurrencies, setAdminCurrencies] = useState([]);
  // console.log("null")

  useEffect(() => {
    API.getAdminCurrencies(
      adminCurrencies => setAdminCurrencies(adminCurrencies),
      undefined
    );
  }, []);
  const currencyObject = getCurrencyAndName(currency, adminCurrencies) || {};

  return currencyObject?.code ? (
    <div className="flex items-center">
      <img
        src={`/images/${(currencyObject.code || currency).toLowerCase()}-square.svg`}
        className={`${iconOnly && 'cursor-pointer'} ${className}`}
        uk-tooltip={iconOnly && (currencyObject.name || currency)}
      />
      {!iconOnly && (
        <span className="-mb-1 ml-3 text-capitalize">
          {currencyObject.name || currency}
        </span>
      )}
    </div>
  ) : null;
}

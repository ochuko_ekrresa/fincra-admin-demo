import React, { Children } from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';

const ActiveLink = ({ router, children, ...props }) => {
  const child = Children.only(children);

  let className = child.props.className || '';
  if (
    router?.asPath === props?.as ||
    (router?.asPath === props?.href && props?.activeClassName)
  ) {
    className = `${className} ${props?.activeClassName}`.trim();
  }

  delete props.activeClassName;
  return (
    <li className="h-full text-gray-400">
      <Link {...props}>{React.cloneElement(child, { className })}</Link>
    </li>
  );
};

const TabLink = withRouter(ActiveLink);
export default function TabNavigation({ links }) {
  return (
    // <div className="uk-margin-medium-top">
    //   <ul className="uk-flex-center" uk-tab="">
    //     {links &&
    //       links.map(({ url, title }) => (
    //         <TabLink
    //           activeClassName="uk-active font-medium text-secondary border-secondary"
    //           href={url}
    //         >
    //           <a className="uk-text-capitalize" style={{ color: "inherit" }}>
    //             {title}
    //           </a>
    //         </TabLink>
    //       ))}
    //   </ul>
    // </div>
    <div className="inline-flex bg-transparent border-gray-300">
      <ul className="flex h-10 items-center justify-center text-sm">
        {links &&
          links.map(({ url, title, as }, idx) => (
            <TabLink
              key={idx}
              href={url}
              as={as}
              activeClassName="border-b-4 border-secondary text-secondary bg-blue-100 rounded-t"
            >
              <a
                className={`mr-6 h-full flex items-center font-medium no-underline p-4 px-6 ${
                  title.length > 4 ? 'capitalize' : 'capitalize'
                }`}
              >
                {title}
              </a>
            </TabLink>
          ))}
      </ul>
    </div>
  );
}

import { render } from '@testing-library/react';
import TabNavigation from './TabNavigation';

const links = [
  { url: '/home', title: 'Home', as: 'home' },
  { url: '/settings', title: 'Settings', as: 'settings' },
];

describe('Tab Navigation', () => {
  it('should render correctly', () => {
    const { container } = render(<TabNavigation links={links} />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="inline-flex bg-transparent border-gray-300"
        >
          <ul
            class="flex h-10 items-center justify-center text-sm"
          >
            <li
              class="h-full text-gray-400"
            >
              <a
                class="mr-6 h-full flex items-center font-medium no-underline p-4 px-6 capitalize"
                href="home"
              >
                Home
              </a>
            </li>
            <li
              class="h-full text-gray-400"
            >
              <a
                class="mr-6 h-full flex items-center font-medium no-underline p-4 px-6 capitalize"
                href="settings"
              >
                Settings
              </a>
            </li>
          </ul>
        </div>
      </div>
    `);
  });
});

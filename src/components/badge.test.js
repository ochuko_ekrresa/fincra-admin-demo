import Badge from './Badge';
import { render } from '@testing-library/react';

describe('Badge Component', () => {
  it('renders correctly', () => {
    const { container, rerender } = render(<Badge type="warning" title="Pending" />);

    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          class="undefined text-yellow-600 border-yellow-600 bg-yellow-100 disabled:text-orange-300 inline-flex py-1 px-4 uppercase border rounded text-xs font-semibold cursor-pointer"
          uk-tooltip=""
        >
          <span
            class="pt-px"
          >
            Pending
          </span>
        </span>
      </div>
    `);

    rerender(<Badge dot type="warning" title="Pending" />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        <span
          class="inline-flex p-2 rounded bg-yellow-600 capitalize"
          uk-tooltip="title:Pending; pos: top"
        />
      </div>
    `);
  });
});

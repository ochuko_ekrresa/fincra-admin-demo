import { render, getByTestId, getAllByText, getByText } from '@testing-library/react';
import user from '@testing-library/user-event';
import Tabs from './Tabs';
import { shallow } from 'enzyme';

const NameComponent = ({ name }) => {
  return <div>Name: {name}</div>;
};

describe('Tabs Component', () => {
  let container, activeTabContainer, menuContainer;
  const names = {
    first: <NameComponent name="John" />,
    second: <NameComponent name="Wole" />,
    third: <NameComponent name="Mike" />,
  };

  beforeEach(() => {
    const { container: wrapper } = render(<Tabs components={names} />);
    container = wrapper;
    activeTabContainer = getByTestId(container, 'active-tab');
    menuContainer = getByTestId(container, 'tabs-menu');
  });

  it('renders correctly', () => {
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div>
          <div
            class="inline-flex"
            data-testid="tabs-menu"
          >
            <ul
              class="flex h-10 items-center justify-center text-sm my-4"
            >
              <li
                class="h-full text-gray-400"
              >
                <button
                  class="mr-6 h-full flex items-center font-medium no-underline p-2 px-6 capitalize border-b-4 border-secondary text-secondary bg-blue-100 rounded-t"
                >
                  first
                </button>
              </li>
              <li
                class="h-full text-gray-400"
              >
                <button
                  class="mr-6 h-full flex items-center font-medium no-underline p-2 px-6 capitalize "
                >
                  second
                </button>
              </li>
              <li
                class="h-full text-gray-400"
              >
                <button
                  class="mr-6 h-full flex items-center font-medium no-underline p-2 px-6 capitalize "
                >
                  third
                </button>
              </li>
            </ul>
          </div>
          <div
            class=""
            data-testid="active-tab"
          >
            <div>
              Name: 
              John
            </div>
          </div>
        </div>
      </div>
    `);
  });

  it('can switch tabs correctly', () => {
    const menuItems = Object.keys(names);

    menuItems.forEach(name => {
      const button = getByText(menuContainer, name);
      const { container: nameContainer } = render(names[name]);
      user.click(button);

      expect(activeTabContainer).toContainHTML(nameContainer.innerHTML);
    });
  });
});

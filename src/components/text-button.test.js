import TextButton from './TextButton';
import { render, getByTestId } from '@testing-library/react';
import user from '@testing-library/user-event';

describe('TextButton Component', () => {
  describe('renders correctly', () => {
    let buttonElement, container, rerender;

    beforeEach(() => {
      const { container: wrapper, rerender: newRerender } = render(
        <TextButton text="Click here" />
      );
      buttonElement = getByTestId(wrapper, 'text-button');
      container = wrapper;
      rerender = newRerender;
    });

    test('with text only', () => {
      expect(container).toMatchInlineSnapshot(`
        <div>
          <button
            class="uppercase undefined outline-none focus:outline-none py-4 px-5 rounded inline-flex items-center justify-center text-sm font-medium undefined w-undefined disabled:bg-transparent"
            data-testid="text-button"
            type="button"
          >
            Click here
          </button>
        </div>
      `);
    });

    test('with text and tooltip', () => {
      rerender(<TextButton text="Pending" title="Click here to show pending stuffs" />);
      expect(container).toMatchInlineSnapshot(`
        <div>
          <button
            class="uppercase undefined outline-none focus:outline-none py-4 px-5 rounded inline-flex items-center justify-center text-sm font-medium undefined w-undefined disabled:bg-transparent"
            data-testid="text-button"
            type="button"
          >
            Pending
          </button>
        </div>
      `);
    });

    it('with disabled', () => {
      rerender(<TextButton text="Click me" disabled />);
      expect(buttonElement).toBeDisabled();
    });

    test('with onClick', () => {
      const mockHandleClick = jest.fn().mockImplementation(event => event.persist());
      rerender(<TextButton text="Click me" onClick={mockHandleClick} />);

      user.click(buttonElement);

      expect(mockHandleClick).toBeCalled();
    });
  });
});

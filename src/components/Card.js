import React from 'react';

export default function Card({ children, className, title, subtitle, align = 'start' }) {
  return (
    <div
      className={`p-6 rounded min-h-10 bg-white flex flex-col items-${align} w-full ${className}`}
    >
      {title && (
        <h2
          className={`text-lg font-medium text-gray-800 ${!subtitle ? 'mb-6' : 'mb-1'}`}
        >
          {title}
        </h2>
      )}
      {subtitle && <span className="opacity-50 text-sm mb-6">{subtitle}</span>}
      {children}
    </div>
  );
}

import Image from 'next/image';
import ReactModal from 'react-modal';

const customStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(42, 34, 68, 0.4)',
    overflowY: 'auto',
    maxHeight: '100vh',
    zIndex: 100,
  },
  content: {
    top: '7%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    transform: 'translate(-50%, 0%)',
    border: 'none',
    marginBottom: '3%',
    overflow: 'visible',
  },
};

ReactModal.setAppElement('body');

export default function Modal({
  isOpen,
  onAfterOpen,
  onAfterClose,
  onRequestClose,
  contentLabel,
  children,
}) {
  return (
    <ReactModal
      isOpen={isOpen}
      onAfterClose={onAfterClose}
      onAfterOpen={onAfterOpen}
      onRequestClose={onRequestClose}
      style={customStyles}
      contentLabel={contentLabel}
    >
      <div className="relative">
        <div className="absolute right-0">
          <span
            className="cursor-pointer bg-[#f7f7fa] grid place-items-center w-8 h-8 rounded-full"
            onClick={onRequestClose}
          >
            <Image
              src="/images/close.svg"
              width={20}
              height={20}
              alt="click to close modal"
              unoptimized
            />
          </span>
        </div>
      </div>
      <div className="flex flex-col text-dark-100 mt-4">{children}</div>
    </ReactModal>
  );
}

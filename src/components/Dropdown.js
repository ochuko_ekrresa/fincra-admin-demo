import React, { useState } from 'react';

export default function Dropdown({
  placeholder,
  options,
  onClick = () => {},
  onSelect = () => {},
}) {
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = () => {
    setIsOpen(!isOpen);
    onClick();
  };

  return (
    <div className="group inline-block relative">
      <button
        data-testid="dropdown-button"
        onClick={handleClick}
        className="bg-secondary hover:bg-blue-900 text-gray-100 font-medium py-4 px-5 rounded inline-flex items-center justify-between outline-none focus:outline-none uppercase text-sm w-56"
      >
        <div className="w-full">
          <span className="mr-1">{placeholder}</span>
        </div>
        <svg
          className="fill-current h-4 w-4"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
        >
          <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
        </svg>
      </button>
      {isOpen && (
        <ul
          data-testid="dropdown-items"
          className={`absolute right-0 text-gray-700 pt-1 ${
            isOpen ? 'block' : 'hidden'
          } w-56 shadow-sm rounded overflow-hidden`}
        >
          {options.map((option, idx) => {
            const { label } = option;

            return (
              <li key={idx}>
                <button
                  className="rounded-t bg-white hover:bg-gray-100 py-3 px-4 block text-center whitespace-no-wrap w-full text-sm"
                  onClick={() => {
                    setIsOpen(!isOpen);
                    onSelect(option);
                  }}
                >
                  {label || option}
                </button>
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
}

import CurrencyCard from './CurrencyCard';
import { render, getByText } from '@testing-library/react';
import API from '../utils/api';

jest.mock('../utils/api');
API.getAdminCurrencies = jest.fn().mockImplementation(success =>
  success([
    {
      name: 'Bitcoin',
      code: 'BTC',
      currencyType: 'crypto',
    },
    {
      name: 'Litecoin',
      code: 'LTC',
      currencyType: 'crypto',
    },
  ])
);

describe('Currency Card', () => {
  let container, rerender;

  beforeEach(() => {
    const { container: wrapper, rerender: newRerender } = render(
      <CurrencyCard currency="btc" />
    );
    container = wrapper;
    rerender = newRerender;
  });

  it('should render correctly', () => {
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="flex items-center"
        >
          <img
            class="undefined undefined"
            src="/images/btc-square.svg"
          />
          <span
            class="-mb-1 ml-3 text-capitalize"
          >
            Bitcoin
          </span>
        </div>
      </div>
    `);
  });

  it('should render icon only', () => {
    rerender(<CurrencyCard currency="btc" iconOnly />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="flex items-center"
        >
          <img
            class="cursor-pointer undefined"
            src="/images/btc-square.svg"
            uk-tooltip="Bitcoin"
          />
        </div>
      </div>
    `);
  });
});

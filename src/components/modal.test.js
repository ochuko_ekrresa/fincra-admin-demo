import Modal from './Modal';
import { render } from '@testing-library/react';

describe('Modal Component', () => {
  test('renders correctly', () => {
    render(
      <Modal title="Sample Title" subtitle="Sample Subtitle" isOpen={true}>
        <p>The children are here!</p>
      </Modal>
    );
    expect(document.querySelector('.ReactModalPortal')).toMatchInlineSnapshot(`
      <div
        class="ReactModalPortal"
      >
        <div
          class="ReactModal__Overlay ReactModal__Overlay--after-open"
          style="position: fixed; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: rgba(42, 34, 68, 0.4); overflow-y: auto; max-height: 100vh;"
        >
          <div
            class="ReactModal__Content ReactModal__Content--after-open"
            role="dialog"
            style="position: absolute; top: 7%; left: 50%; background: rgb(255, 255, 255); overflow: visible; border-radius: 4px; outline: none; padding: 20px; transform: translate(-50%, 0%); margin-bottom: 3%;"
            tabindex="-1"
          >
            <div
              class="m-3 w-full md:w-sm"
            >
              <div
                class="flex justify-between"
              >
                <div
                  class="flex flex-col"
                >
                  <h3
                    class="text-lg font-medium text-primarydark capitalize mb-px"
                  >
                    Sample Title
                  </h3>
                  <span
                    class="text-sm"
                  >
                    Sample Subtitle
                  </span>
                </div>
                <span
                  class="cursor-pointer"
                >
                  <img
                    src="/images/close.svg"
                  />
                </span>
              </div>
            </div>
            <div
              class="flex flex-col m-3 text-primarydark md:w-sm"
            >
              <p>
                The children are here!
              </p>
            </div>
          </div>
        </div>
      </div>
    `);
  });
});

import { render, getByTestId } from '@testing-library/react';
import user from '@testing-library/user-event';
import SettingsItem from './SettingsItem';

const mockOnChange = jest.fn();
describe('Settings Item', () => {
  let container;
  beforeEach(() => {
    const { container: wrapper } = render(
      <SettingsItem
        type="checkbox"
        name="useCurrenciesInWalletSettings"
        title="Accepted Currency"
        checked={false}
        description="Receive payments with my preferred currencies."
        onChange={mockOnChange}
      />
    );
    container = wrapper;
  });

  it('should call onChange when changed', () => {
    user.click(getByTestId(container, 'useCurrenciesInWalletSettings'));
    expect(mockOnChange).toHaveBeenCalled();
  });

  it('should render correctly', () => {
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="undefined flex flex-col text-sm w-full py-2 my-px "
        >
          <h3
            class="font-medium mb-px capitalize false"
          >
            Accepted Currency
          </h3>
          <div
            class="flex justify-between "
          >
            <span
              class="opacity-50"
            >
              Receive payments with my preferred currencies.
            </span>
            <div
              class="CheckBox flex flex-col text-primarydark text-sm undefined"
            >
              <label
                class="text-primarydark"
              >
                <input
                  class="mr-3 leading-tight"
                  data-testid="useCurrenciesInWalletSettings"
                  name="useCurrenciesInWalletSettings"
                  title="Accepted Currency"
                  type="checkbox"
                />
                <span
                  class="text-sm hidden"
                >
                  Accepted Currency
                </span>
                <span
                  class="checkmark bg-current"
                />
              </label>
            </div>
          </div>
        </div>
      </div>
    `);
  });
});

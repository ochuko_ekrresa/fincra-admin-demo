import React from 'react';
import { RadioGroup, CheckBox } from '../Form';

export default function SettingsItem({
  checked,
  className,
  description,
  disabled,
  name,
  onChange,
  options,
  orientation = 'horizontal',
  title,
  type = 'heading',
  required,
  value,
}) {
  return (
    <div
      className={`${className} flex flex-${
        !description ? 'row justify-between' : 'col'
      } text-sm w-full py-2 my-px ${type === 'heading' ? 'border-b pb-4' : ''}`}
    >
      <h3 className={`font-medium mb-px capitalize ${type === 'heading' && 'text-base'}`}>
        {title}
      </h3>
      <div className={`flex justify-between ${type === 'radio' ? 'flex-col' : ''}`}>
        {description && <span className="opacity-50">{description}</span>}
        {type === 'checkbox' && (
          <CheckBox
            hideTitle
            name={name}
            title={title}
            type="settings"
            onChange={onChange}
            checked={checked}
            disabled={disabled}
            required={required}
          />
        )}
        {type === 'radio' && (
          <div className="my-3">
            <RadioGroup
              name={name}
              selectedOption={value}
              options={options}
              onChange={onChange}
              orientation={orientation}
            />
          </div>
        )}
      </div>
    </div>
  );
}

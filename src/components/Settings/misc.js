import React, { useState, useEffect } from 'react';
import API from '../../utils/api';
import Card from '../Card';
import { Input } from '../Form';
import Button from '../Button';
import { Grid } from '../Layout';
import Toast from '../../utils/toast';

export default function MiscSettings() {
  const [settings, setSettings] = useState({});

  const updateSettings = evt => {
    evt.preventDefault();

    API.updateSettings(
      settings,
      () => {
        Toast({
          title: 'Success',
          message: 'Settings updated!',
          type: 'success',
        });
      },
      error => {
        Toast({
          title: 'Error',
          message: error.error,
          type: 'danger',
        });
      }
    );
  };

  useEffect(() => {
    API.getSettings(async settings => {
      setSettings({
        quoteTime: settings.quoteTime,
      });
    });
  }, []);

  return (
    settings && (
      <Card title="Miscelleneous" className="md:w-3xl mb-4">
        <div className="w-full">
          <form onSubmit={updateSettings} className="flex justify-between items-end">
            <Grid columns={2} gap="0.5rem">
              <Input
                autoComplete="off"
                name="quoteTime"
                title="Quote Time (seconds)"
                type="number"
                placeholder="Quote Time in seconds"
                onChange={evt =>
                  setSettings({
                    ...settings,
                    quoteTime: evt.target.value,
                  })
                }
                wrapperClass="flex-1 mr-4"
                value={settings.quoteTime}
              />
            </Grid>
            <Button type="secondary" padding={3} width="32" text="Save" />
          </form>
        </div>
      </Card>
    )
  );
}

import React, { useState, useEffect } from 'react';
import Card from '../Card';
import API from '../../utils/api';
import { camelCaseToWords, capitalizeFirstLetter } from '../../helpers';
import { Input } from '../Form';
import { Button } from '../index';
import Toast from '../../utils/toast';

const VolatilityQuotient = () => {
  const [channels, setChannels] = useState(null);
  const [existingVolatilityQuotients, setExistingVolatilityQuotients] = useState([]);
  const [formData, setFormData] = useState({});

  const handleChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const handleSuccess = key =>
    Toast({
      title: `Success: ${key}`,
      message: `${capitalizeFirstLetter(key)} volatility quotient saved!`,
      type: 'success',
    });

  const handleError = (error, key) =>
    Toast({
      title: `Error: ${key}`,
      message: error.error,
      type: 'danger',
    });

  const handleSubmit = e => {
    e.preventDefault();

    Object.keys(formData).forEach(key => {
      const payload = { channelName: key, value: formData[key] };
      const exists = existingVolatilityQuotients.find(
        quotient => quotient.channelName === key
      );

      if (!exists) {
        API.createVolatilityQuotient(
          payload,
          () => handleSuccess(key),
          error => handleError(error, key)
        );
      } else if (exists && exists.value !== +formData[key]) {
        API.updateVolatilityQuotient(
          exists._id,
          payload,
          () => handleSuccess(key),
          error => handleError(error, key)
        );
      }
    });

    // fetch latest after saving VQs.
    API.getVolatilityQuotients(data => setExistingVolatilityQuotients(data.data));
  };

  useEffect(() => {
    API.getChannels(channels => setChannels(channels));
    API.getVolatilityQuotients(data => {
      const quotients = data.data;
      const initialState = quotients.reduce(
        (acc, quotient) => ({ ...acc, [quotient.channelName]: quotient.value }),
        {}
      );
      setFormData(initialState);
      setExistingVolatilityQuotients(quotients);
    });
  }, []);

  return (
    <Card title="Volatility Quotients" className="md:w-3xl mb-4">
      <form onSubmit={handleSubmit} className={'w-full space-y-6'}>
        <h4 className={'font-medium text-sm'}>Channels</h4>
        <div className={'grid grid-cols-3 gap-6'}>
          {channels?.map(channel => (
            <div key={channel.name}>
              <Input
                type={'number'}
                step={0.01}
                title={`${camelCaseToWords(channel.name, 0)} ${
                  !channel.active ? '(Inactive)' : ''
                }`}
                onChange={handleChange}
                name={channel.name}
                value={formData[channel.name]}
                placeholder={'Quotient (%)'}
              />
            </div>
          ))}
        </div>
        <div>
          <Button type={'secondary'} text={'Save'} />
        </div>
      </form>
    </Card>
  );
};

export default VolatilityQuotient;

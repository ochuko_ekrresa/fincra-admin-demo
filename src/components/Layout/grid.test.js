import { render } from '@testing-library/react';
import Grid from './Grid';

describe('Grid component', () => {
  it('should render correctly', () => {
    const { container, rerender } = render(
      <Grid columns={3}>
        <div>Col 1</div>
        <div>Col 2</div>
        <div>Col 3</div>
      </Grid>
    );
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="grid undefined"
          style="display: grid; grid-template-columns: repeat(3, 1fr);"
        >
          <div>
            Col 1
          </div>
          <div>
            Col 2
          </div>
          <div>
            Col 3
          </div>
        </div>
      </div>
    `);
    rerender(
      <Grid columns={3} notFluid>
        <div>Col 1</div>
        <div>Col 2</div>
        <div>Col 3</div>
      </Grid>
    );
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="false undefined"
          style="display: grid; grid-template-columns: repeat(3, 1fr);"
        >
          <div>
            Col 1
          </div>
          <div>
            Col 2
          </div>
          <div>
            Col 3
          </div>
        </div>
      </div>
    `);
  });
});

import { render } from '@testing-library/react';
import DashboardLayout from './DashboardLayout';

describe('Dashboard Layout component', () => {
  it('should render correctly', () => {
    const { container } = render(
      <DashboardLayout title="Dashboard" subtitle="Welcome to your dashboard">
        <p>I am a child</p>
      </DashboardLayout>
    );
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="dashboard"
        >
          <div
            class="flex"
          >
            <nav
              class="md:flex flex-col w-72 overflow-y-auto h-screen sticky top-0 left-0 hidden"
              style="background-color: rgb(248, 247, 252);"
            >
              <div
                class="flex items-center w-full h-20 px-8 flex-shrink-0"
                style="background: rgba(255, 255, 255, 0.5);"
              >
                <img
                  class="h-8"
                  src="/images/logo-primary.svg"
                />
              </div>
              <div
                class=""
              >
                <div
                  class="flex flex-col py-4"
                >
                  <span
                    class="px-8 categoryname text-sm font-medium text-secondary mb-4 capitalize"
                  />
                  <ul>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/overview"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Overview
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/businesses"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Businesses
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/merchants"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Merchants
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div
                  class="flex flex-col py-4"
                >
                  <span
                    class="px-8 categoryname text-sm font-medium text-secondary mb-4 capitalize"
                  >
                    Transactions
                  </span>
                  <ul>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/collections"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Collections
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/disbursements"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Disbursements
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/order-book"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Order Book
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/quotes"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Quotes
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div
                  class="flex flex-col py-4"
                >
                  <span
                    class="px-8 categoryname text-sm font-medium text-secondary mb-4 capitalize"
                  >
                    More
                  </span>
                  <ul>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/corridor-management"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Corridor Mngt.
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/exchange-rate-management"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Exchange Rate Mngt.
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/settlement-config"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Settlement Confg.
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/settings"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/payment-link.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Settings
                        </span>
                      </a>
                    </li>
                    <li
                      class=""
                    >
                      <a
                        class="flex items-center py-2 text-primarydark-1 hover:text-secondary hover:bg-white cursor-pointer capitalize px-8"
                        href="/auth/logout"
                      >
                        <span
                          class="icon w-8 -mt-1"
                        >
                          <img
                            src="/images/refund.svg"
                          />
                        </span>
                        <span
                          class=""
                        >
                          Sign out
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            <div
              class="w-5/6 flex flex-col flex-1"
            >
              <div
                class="header h-20 flex justify-end items-center w-full px-8 shadow-sm"
              />
              <div
                class="bg-gray-100 min-h-screen p-6 md:px-12 py-8 text-primarydark"
              >
                <div
                  class="pb-2"
                >
                  <h1
                    class="text-xl md:text-2xl font-medium"
                  >
                    Dashboard
                  </h1>
                  <p
                    class=""
                  >
                    Welcome to your dashboard
                  </p>
                </div>
                <p>
                  I am a child
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
  });
});

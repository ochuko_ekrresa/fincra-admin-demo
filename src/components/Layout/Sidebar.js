import { signOut } from 'next-auth/client';

import { NavLink } from '../';
import navLinks from '../../constants/navigation';

export default function Sidebar() {
  return (
    <nav className="sticky top-0 left-0 flex-col hidden h-screen overflow-y-auto md:flex bg-white-100 w-72">
      <div className="flex items-center flex-shrink-0 w-full px-8 py-6">
        <img className="w-32" src="/images/logo-primary.svg" />
        <p className="bg-white ml-4 p-1 rounded-default text-[0.6rem] uppercase">admin</p>
      </div>

      <div className="">
        <div className="my-4">
          <NavLink
            path="/overview"
            imgPath="/images/overview.svg"
            activeImgPath="/images/overview-active.svg"
          >
            Overview
          </NavLink>
        </div>

        {navLinks.map(({ links, category }, idx) => (
          <div className="flex flex-col pt-10 pb-4" key={idx}>
            <span className="px-8 mb-2 text-sm font-medium uppercase text-light">
              {category}
            </span>
            <ul>
              {links.map(link => (
                <NavLink
                  key={link.name}
                  path={link.route}
                  imgPath={link.icon}
                  activeImgPath={link.activeIcon}
                >
                  {link.name}
                </NavLink>
              ))}
            </ul>
          </div>
        ))}
      </div>

      <div className="px-8 py-8">
        <button
          className="bg-white border border-[#f2f2f2] py-2 w-full shadow-default text-dark-100 text-lg flex items-center justify-center"
          onClick={() => {
            signOut({ redirect: false });
          }}
        >
          <span>Sign Out</span>
          <img className="w-6 ml-3" src="/images/sign-out.svg" />
        </button>
      </div>
    </nav>
  );
}

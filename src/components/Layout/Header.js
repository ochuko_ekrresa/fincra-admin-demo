import React from 'react';

export default function Header() {
  return (
    <div className="flex items-center justify-end h-20 px-8 relative shadow-header z-10"></div>
  );
}

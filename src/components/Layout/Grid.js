import React from 'react';

export default function Grid({ children, className, columns, gap, notFluid }) {
  return (
    <div
      className={`${!notFluid && 'grid'} ${className}`}
      style={{
        display: 'grid',
        gridTemplateColumns: `repeat(${columns}, 1fr)`,
        gap,
      }}
    >
      {children}
    </div>
  );
}

import { render } from '@testing-library/react';
import AuthLayout from './AuthLayout';

describe('Auth Layout component', () => {
  it('should render correctly', () => {
    const { container } = render(
      <AuthLayout title="Login" subtitle="Welcome back">
        <p>I am a child</p>
      </AuthLayout>
    );
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="h-full flex flex-col"
        >
          <nav
            class="flex items-center w-full p-6 h-20 bg-white sticky top-0 z-10"
          >
            <img
              alt="Fliqpay"
              class="h-8 cursor-pointer"
              src="/images/logo-primary.svg"
            />
          </nav>
          <div
            class="flex justify-center w-full flex-1 md:w-full mt-6 md:mt-12"
          >
            <div
              class="w-full bg-white flex flex-col p-6 md:p-0 items-center text-sm text-primarydark pb-8"
            >
              <div
                class="flex flex-col w-full md:w-sm"
              >
                <div
                  class="flex flex-col items-center my-8 font-medium text-center"
                >
                  <h1
                    class="text-3xl md:text-4xl text-secondary mb-1"
                  >
                    Login
                  </h1>
                  <p
                    class="text-base font-normal text-primarydark my-1"
                  >
                    Welcome back
                  </p>
                </div>
                <div
                  class="w-full"
                >
                  <p>
                    I am a child
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    `);
  });
});

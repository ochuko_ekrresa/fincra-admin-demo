import { useEffect, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';

import Header from './Header';
import PageLoader from '../Loaders/PageLoader';
import Sidebar from './Sidebar';

function DashboardLayout({ children, title, subtitle }) {
  const [pageLoading, setPageLoading] = useState(false);
  const router = useRouter();

  const routeChangeStart = () => {
    setPageLoading(true);
  };

  const routeChangeEnd = () => {
    setPageLoading(false);
  };

  useEffect(() => {
    router.events.on('routeChangeStart', routeChangeStart);
    router.events.on('routeChangeComplete', routeChangeEnd);
    router.events.on('routeChangeError', routeChangeEnd);

    return () => {
      router.events.off('routeChangeStart', routeChangeStart);
      router.events.off('routeChangeComplete', routeChangeEnd);
      router.events.off('routeChangeError', routeChangeEnd);
    };
  }, [router.events]);

  return (
    <div className="dashboard">
      <Head>
        <title>{title}</title>
      </Head>

      <div className="flex">
        <Sidebar />
        <div className="flex flex-col flex-1 w-5/6">
          <Header />
          <section className="relative p-6 py-8 bg-white min-h-full-5rem md:px-12 text-dark-100">
            {pageLoading ? <PageLoader /> : null}

            <div className="pb-2">
              <h1 className="text-xl font-medium md:text-3xl">{title}</h1>
              <p className="">{subtitle}</p>
            </div>
            {children}
          </section>
        </div>
      </div>
    </div>
  );
}

export default DashboardLayout;

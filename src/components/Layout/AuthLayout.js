import React from 'react';
import Head from 'next/head';
import Link from 'next/link';

import * as ROUTES from '../../constants/routes';

export default function AuthLayout({
  title,
  subtitle,
  icon,
  sideImage,
  columnSize = sideImage ? '3/5' : 'full',
  innerColumnSize = 'sm',
  children,
}) {
  // Calculate column size for the side image
  const getSideColumnSize = () => {
    const splitColumnSize = columnSize.split('/');
    return `${splitColumnSize[1] - splitColumnSize[0]}/${splitColumnSize[1]}`;
  };

  return (
    <div className="h-full flex flex-col">
      <Head>
        <title>Login</title>
      </Head>
      <nav className="flex items-center w-full p-6 h-20 bg-white sticky top-0 z-10">
        <Link href={ROUTES.LANDING} passHref>
          <img
            src={'/images/logo-primary.svg'}
            alt="Fliqpay"
            className="h-8 cursor-pointer"
          />
        </Link>
      </nav>
      <div className={`flex justify-center w-full flex-1 md:w-3/5 mt-6 md:mt-12`}>
        <div className="w-full md:w-3/5 bg-white flex flex-col p-6 md:p-0 items-center text-sm text-primary-dark pb-8">
          <div className="flex flex-col w-full md:w-96">
            <div className="flex flex-col items-center my-8 font-medium text-center">
              {icon && <img src={icon} className="my-8 w-32 md:w-auto" alt={title} />}
              <h1 className="text-3xl md:text-4xl text-secondary mb-1">{title}</h1>
              <p className="text-base font-normal text-dark-100 my-1">{subtitle}</p>
            </div>
            <div className="w-full">{children}</div>
          </div>
        </div>
        {sideImage && (
          <div className="w-2/5 bg-blue-100 h-screen fixed right-0 top-0 z-20 hidden md:block">
            <img src={sideImage} className="object-cover h-full w-full" />
          </div>
        )}
      </div>
    </div>
  );
}

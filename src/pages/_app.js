import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { Provider as NextAuthProvider, useSession } from 'next-auth/client';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Hydrate } from 'react-query/hydration';
import { ReactQueryDevtools } from 'react-query/devtools';
import { Toaster } from 'react-hot-toast';

import PageLoader from '../components/Loaders/PageLoader';
import Provider from '../contexts/provider';

import '../styles/index.scss';

const queryClient = new QueryClient();

export default function App({ Component, pageProps }) {
  const getLayout = Component.getLayout ?? (page => page);

  /**
   * Add a protected property set to true to a page to prevent unauthorized access
   */
  const isPageProtected = Boolean(Component.protected);

  return (
    <NextAuthProvider session={pageProps.session}>
      <QueryClientProvider client={queryClient}>
        <Hydrate state={pageProps.dehydratedState}>
          <Provider>
            {isPageProtected ? (
              <Auth>{getLayout(<Component {...pageProps} />)}</Auth>
            ) : (
              getLayout(<Component {...pageProps} />)
            )}
          </Provider>
          <Toaster />
        </Hydrate>
        <ReactQueryDevtools />
      </QueryClientProvider>
    </NextAuthProvider>
  );
}

function Auth({ children }) {
  const [session, loading] = useSession();
  const router = useRouter();

  const isAdmin = Boolean(session?.user?.isAdmin);

  useEffect(() => {
    if (loading) return;

    if (!isAdmin) router.push('/auth/login');
  }, [isAdmin, loading, router]);

  if (isAdmin) {
    return children;
  }

  /**
   * Session is being fetched
   * If no user or user is not admin, useEffect() will redirect to login page
   */
  return (
    <div>
      <PageLoader />
    </div>
  );
}

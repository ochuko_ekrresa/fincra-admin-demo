import { useEffect } from 'react';

import * as ROUTES from '../constants/routes';
import { useRouter } from 'next/router';

export default function Index() {
  const router = useRouter();

  useEffect(() => {
    router.push(ROUTES.DASHBOARD.HOME);
  }, [router]);

  return null;
}

Index.protected = true;

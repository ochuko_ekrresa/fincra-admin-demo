import NextAuth from 'next-auth';
import Providers from 'next-auth/providers';
import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.BACKEND_URL,
  headers: { 'api-key': process.env.API_KEY },
});

const options = {
  providers: [
    Providers.Credentials({
      name: 'Local Auth',
      async authorize(credentials) {
        try {
          const response = await axiosInstance.post('/profile/auth/login', {
            email: credentials.email,
            password: credentials.password,
          });

          if (response) {
            return response.data.data;
          }

          return null;
        } catch (error) {
          const errorMessage = error?.response?.data?.error || error.message;
          throw new Error(errorMessage);
        }
      },
    }),
  ],
  callbacks: {
    async signIn({ user }) {
      if (user.isAdmin) {
        return true;
      }

      return false;
    },

    async jwt(token, user) {
      if (user) {
        token.accessToken = user.token;
        token.user = user.user;
      }

      return token;
    },

    async session(session, token) {
      session.accessToken = token.accessToken;
      session.user = token.user;

      return session;
    },
  },
  pages: { signIn: '/auth/login', error: '/auth/login' },
};

const NextAuthConfig = (req, res) => NextAuth(req, res, options);

export default NextAuthConfig;

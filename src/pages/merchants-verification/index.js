import { DashboardLayout } from '../../components/Layout';

function MerchantVerification() {
  return <div></div>;
}

MerchantVerification.getLayout = page => (
  <DashboardLayout title="Merchant Verification">{page}</DashboardLayout>
);

MerchantVerification.protected = true;

export default MerchantVerification;

import { DashboardLayout } from '../../components/Layout';

export default function Conversion() {
  return <div></div>;
}

Conversion.getLayout = page => (
  <DashboardLayout title="Conversion">{page}</DashboardLayout>
);

Conversion.protected = true;

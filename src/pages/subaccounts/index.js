import { DashboardLayout } from '../../components/Layout';

function SubAccounts() {
  return <div></div>;
}

SubAccounts.getLayout = page => (
  <DashboardLayout title="Subaccounts">{page}</DashboardLayout>
);

SubAccounts.protected = true;

export default SubAccounts;

import React from 'react';
import { DashboardLayout } from '../../components/Layout';
import { TabNavigation } from '../../components/Navigation';
import { SettingsNavigation } from '../../constants/navigation';
import MiscSettings from '../../components/Settings/misc';

export default function Settings() {
  return (
    <div className="flex flex-col">
      <div className="flex justify-center md:w-3/4 mb-6">
        <TabNavigation links={SettingsNavigation} />
      </div>
      <div className="flex">
        <div className="flex flex-col items-center w-full md:w-3/4 mr-4">
          <MiscSettings />
        </div>
      </div>
    </div>
  );
}

Settings.getLayout = page => <DashboardLayout title="Settings">{page}</DashboardLayout>;

Settings.protected = true;

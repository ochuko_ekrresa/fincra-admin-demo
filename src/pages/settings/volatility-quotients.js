import React from 'react';
import { withAuthSync } from '../../utils/api';
import { DashboardLayout } from '../../components/Layout';

import { TabNavigation } from '../../components/Navigation';

import { SettingsNavigation } from '../../constants/navigation';
import VolatilityQuotient from '../../components/Settings/VolatilityQuotient';

function VolatilityQuotientsPage() {
  return (
    <DashboardLayout title="Settings">
      <div className="flex flex-col">
        <div className="flex justify-center md:w-3/4 mb-6">
          <TabNavigation links={SettingsNavigation} />
        </div>
        <div className="flex">
          <div className="flex flex-col items-center w-full space-y-6 md:w-3/4 mr-4">
            <VolatilityQuotient />
          </div>
        </div>
      </div>
    </DashboardLayout>
  );
}

export default withAuthSync(VolatilityQuotientsPage);

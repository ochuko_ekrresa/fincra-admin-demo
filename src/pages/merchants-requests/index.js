import { DashboardLayout } from '../../components/Layout';

function MerchantRequests() {
  return <div></div>;
}

MerchantRequests.getLayout = page => (
  <DashboardLayout title="Merchant Requests">{page}</DashboardLayout>
);

MerchantRequests.protected = true;

export default MerchantRequests;

import React from 'react';
import { DashboardLayout } from '../../components/Layout';

function Overview() {
  return <div></div>;
}

Overview.getLayout = page => <DashboardLayout title="Overview">{page}</DashboardLayout>;

Overview.protected = true;

export default Overview;

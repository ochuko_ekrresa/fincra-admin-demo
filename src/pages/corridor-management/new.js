import React, { useEffect } from 'react';
import { DashboardLayout } from '../../components/Layout';
import { withAuthSync } from '../../utils/api';
import NewCorridorStructureForm from '../../components/corridor-management/new-corridor-structure-form';

function NewCorridorManagement({ token }) {
  useEffect(() => {}, []);

  return (
    <DashboardLayout title="New Corridor Structure">
      <div className="flex my-4">
        <NewCorridorStructureForm />
      </div>
    </DashboardLayout>
  );
}

export default withAuthSync(NewCorridorManagement);

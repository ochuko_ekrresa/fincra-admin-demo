import React, { useEffect, useState } from 'react';
import { DashboardLayout } from '../../../components/Layout';
import API, { withAuthSync } from '../../../utils/api';
import UpdateCorridorStructureForm from '../../../components/corridor-management/update-corridor-structure-form';
import { useRouter } from 'next/router';

function NewCorridorManagement({ token }) {
  const router = useRouter();
  const [corridor, setCorridor] = useState(null);

  useEffect(() => {
    const corridorId = router.query.id;

    API.getCorridorById(
      corridorId,
      data => setCorridor(data),
      () => router.push('/corridor-management')
    );
  }, []);

  return (
    <DashboardLayout title="Update Corridor Structure">
      <div className="flex my-4">
        {corridor && <UpdateCorridorStructureForm data={corridor} />}
      </div>
    </DashboardLayout>
  );
}

export default withAuthSync(NewCorridorManagement);

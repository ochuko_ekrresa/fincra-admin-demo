import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { DashboardLayout } from '../../components/Layout';
import API from '../../utils/api';
import { Button } from '../../components';
import Table from '../../components/Table';
import { dateFormat } from '../../helpers';
import * as ROUTES from '../../constants/routes';
import CorridorDetails from '../../components/corridor-management/CorridorDetails';

const Fetching = () => (
  <div className="h-full w-full flex justify-center items-center my-8">
    <div className="w-2/3 h-xs rounded p-8 flex flex-col justify-center items-center">
      <h2 className="text-center text-gray-600 text-base font-medium mb-1">
        Fetching corridors...
      </h2>
    </div>
  </div>
);

export default function CorridorManagement() {
  const [corridors, setCorridors] = useState([]);
  const [error, setError] = useState(null);
  const [isFetching, setIsFetching] = useState(true);

  useEffect(() => {
    setIsFetching(true);
    API.getCorridors(
      data => {
        setCorridors(data);
        setIsFetching(false);
      },
      error => {
        setError(error);
        setIsFetching(false);
      }
    );
  }, []);

  return (
    <>
      <div className="flex justify-end my-4">
        <Link href={ROUTES.DASHBOARD.CORRIDOR_MANAGEMENT} passHref>
          <Button type="secondary" text="New Corridor" />
        </Link>
        {/* <NewCorridorStructure
          onCreate={(data) => setCorridors([...corridors.reverse(), data])}
        /> */}
      </div>
      <div className="my-4">
        {isFetching ? (
          <Fetching />
        ) : (
          <Table
            pagination={true}
            expandableRows={true}
            expandOnRowClicked={true}
            expandableRowsComponent={<CorridorDetails />}
            columns={[
              {
                name: 'Source Currency',
                selector: 'sourceCurrency',
                sortable: true,
              },
              {
                name: 'Destination Currency',
                selector: 'destinationCurrency',
                sortable: true,
              },
              {
                name: 'Transaction Type',
                selector: 'transactionType',
                sortable: true,
              },
              {
                name: 'Date',
                selector: 'createdAt',
                sortable: true,
                cell: row => dateFormat(row.createdAt),
              },
            ]}
            data={corridors}
          />
        )}
      </div>
    </>
  );
}

CorridorManagement.getLayout = page => (
  <DashboardLayout title="Corridor Management">{page}</DashboardLayout>
);

CorridorManagement.protected = true;

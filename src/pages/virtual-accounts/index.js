import { DashboardLayout } from '../../components/Layout';

function VirtualAccounts() {
  return <div></div>;
}

VirtualAccounts.getLayout = page => (
  <DashboardLayout title="Virtual Accounts">{page}</DashboardLayout>
);

VirtualAccounts.protected = true;

export default VirtualAccounts;

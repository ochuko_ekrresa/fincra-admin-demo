import Image from 'next/image';
import { useRouter } from 'next/router';
import { format } from 'date-fns';

import DashboardLayout from '../../../components/Layout/DashboardLayout';
import { DASHBOARD } from '../../../constants/routes';

export default function TopUpHistoryDetails() {
  const router = useRouter();

  return (
    <div>
      <div className="flex items-start">
        <Image
          src="/images/back-icon.svg"
          width={27}
          height={27}
          unoptimized
          alt="Click to go back"
          className="cursor-pointer"
          onClick={
            history.length > 2 ? router.back : () => router.push(DASHBOARD.WALLET_TOP_UPS)
          }
        />
        <h2 className="ml-2 text-2xl font-medium">Viewing Wallet Top-up</h2>
        <div className="bg-white-400 font-light ml-auto px-7 py-4 rounded text-dark-100">
          <span className="mr-2">Approved</span>
          {format(new Date(), 'dd/MM/yyyy HH:mm bbb')}
        </div>
      </div>

      <div className="mt-6 border border-white-200 rounded-[4px] shadow-header px-8">
        <div className="flex flex-wrap">
          {new Array(7).fill('yah').map((_, index) => (
            <div className="py-6  mr-24 last:mr-0" key={index}>
              <h4 className="mb-2 text-dark-100 text-sm">Merchant Name</h4>
              <p className="text-light text-sm whitespace-nowrap">Akintola & Company</p>
            </div>
          ))}
        </div>
      </div>

      <h3 className="mt-9 font-medium">Proof of Payment</h3>
      <p className="mt-2 text-sm text-light">Here’s info uploaded as proof of payment</p>

      <div className="mt-6 border border-white-200 rounded-[4px] shadow-header px-8 pb-10 pt-8">
        <div className="flex">
          {new Array(4).fill('yah').map((_, index) => (
            <div className="mr-24 last:mr-0" key={index}>
              <h4 className="mb-1 text-dark-100 text-sm">Merchant Name</h4>
              <p className="text-light text-sm whitespace-nowrap">Akintola & Company</p>
            </div>
          ))}
        </div>
      </div>

      <h3 className="mt-9 font-medium">Documents Uploaded</h3>
      <p className="mt-2 text-sm text-light">
        Here are the documents Merchant uploaded as proof of payment
      </p>

      <div className="flex flex-wrap">
        {new Array(4).fill('yah').map((_, index) => (
          <div
            key={index}
            className="mt-6 mr-6 border border-white-200 w-max rounded-[4px] shadow-header px-4 py-6 flex items-center cursor-pointer"
          >
            <Image src="/images/sheet.svg" width={40} height={40} alt="" unoptimized />
            <div className="ml-4 mr-12">
              <h4 className="mb-1 text-dark-100 text-sm">Transfer of receipt.png</h4>
              <p className="text-light text-sm whitespace-nowrap">Document Uploaded</p>
            </div>
            <Image src="/images/download.svg" width={40} height={40} alt="" unoptimized />
          </div>
        ))}
      </div>
    </div>
  );
}

TopUpHistoryDetails.getLayout = page => <DashboardLayout>{page}</DashboardLayout>;

TopUpHistoryDetails.protected = true;

import { useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { BsArrowRightShort } from 'react-icons/bs';

import { DashboardLayout } from '../../components/Layout';
import Tabs from '../../components/Tabs';
import RequestTable from '../../components/wallet-top-ups/RequestTable';
import HistoryTable from '../../components/wallet-top-ups/HistoryTable';
import { Input } from '../../components/Form';

export default function WalletTopUps() {
  const router = useRouter();

  const [searchText, setSearchText] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(10);

  return (
    <div>
      <div className="flex items-center justify-between">
        <div className="text-light">Showing {rowsPerPage} top-up requests</div>

        <button className="bg-secondary absolute right-12 top-8 flex items-center px-4 py-3 rounded self-end text-white text-[0.8rem]">
          <span>Fund Wallet</span>
          <BsArrowRightShort className="ml-2 text-2xl" />
        </button>
      </div>

      <div className="flex relative mt-4">
        <Tabs
          components={{
            'Top-Up Requests': (
              <RequestTable
                data={data}
                onRowClickHandler={e => {
                  router.push(router.pathname + '/' + e.id);
                }}
                rowPerPageMonitor={setRowsPerPage}
              />
            ),
            'Approval History': (
              <div>
                <HistoryTable
                  data={data}
                  onRowClickHandler={e => {
                    router.push(router.pathname + '/history/' + e.id);
                  }}
                />
              </div>
            ),
          }}
        />

        <div className="flex absolute right-0 top-3">
          <Input
            inputClass="shadow-default focus:border-dark focus:shadow-none"
            onChange={evt => {
              setSearchText(evt.currentTarget.value);
            }}
            placeholder="Search..."
            isSearch
          />
          <button className="bg-white border border-[#f2f2f2] py-1 shadow-default text-dark-100 text-sm flex items-center px-4 ml-4 rounded">
            <span className="mr-3 text-light">Export</span>
            <Image src="/images/export.svg" width={17} height={17} alt="" unoptimized />
          </button>
        </div>
      </div>
    </div>
  );
}

WalletTopUps.getLayout = page => (
  <DashboardLayout title="Wallet Top-Ups">{page}</DashboardLayout>
);

WalletTopUps.protected = true;

const data = [
  {
    id: 1,
    date_initiated: '14/03/2020',
    merchant_name: 'Topicware',
    currency: 'EUR',
    amount: '$5714867.46',
    status: 'Mazda',
  },
  {
    id: 2,
    date_initiated: '04/07/2019',
    merchant_name: 'Youbridge',
    currency: 'PLN',
    amount: '$518521.51',
    status: 'Aston Martin',
  },
  {
    id: 3,
    date_initiated: '06/02/2021',
    merchant_name: 'Devshare',
    currency: 'CNY',
    amount: '$7244640.35',
    status: 'Lincoln',
  },
  {
    id: 4,
    date_initiated: '11/06/2021',
    merchant_name: 'Tagopia',
    currency: 'CNY',
    amount: '$8691929.23',
    status: 'Dodge',
  },
  {
    id: 5,
    date_initiated: '13/02/2020',
    merchant_name: 'Yakijo',
    currency: 'CNY',
    amount: '$3319584.22',
    status: 'Nissan',
  },
  {
    id: 6,
    date_initiated: '27/11/2020',
    merchant_name: 'Skiba',
    currency: 'RSD',
    amount: '$7184781.19',
    status: 'Cadillac',
  },
  {
    id: 7,
    date_initiated: '23/06/2021',
    merchant_name: 'Centimia',
    currency: 'EUR',
    amount: '$222407.42',
    status: 'Lincoln',
  },
  {
    id: 8,
    date_initiated: '09/12/2020',
    merchant_name: 'Riffpedia',
    currency: 'AZN',
    amount: '$7190505.17',
    status: 'Ford',
  },
  {
    id: 9,
    date_initiated: '22/02/2020',
    merchant_name: 'Kazio',
    currency: 'CNY',
    amount: '$5273654.83',
    status: 'Mazda',
  },
  {
    id: 10,
    date_initiated: '09/08/2020',
    merchant_name: 'Jabbersphere',
    currency: 'BRL',
    amount: '$4157436.01',
    status: 'Mazda',
  },
  {
    id: 11,
    date_initiated: '01/09/2020',
    merchant_name: 'Tagcat',
    currency: 'CNY',
    amount: '$7147799.40',
    status: 'Honda',
  },
  {
    id: 12,
    date_initiated: '19/05/2020',
    merchant_name: 'Skivee',
    currency: 'CNY',
    amount: '$9615957.78',
    status: 'Nissan',
  },
  {
    id: 13,
    date_initiated: '29/12/2020',
    merchant_name: 'Meezzy',
    currency: 'IDR',
    amount: '$1600067.11',
    status: 'Subaru',
  },
  {
    id: 14,
    date_initiated: '05/09/2020',
    merchant_name: 'Skiptube',
    currency: 'MNT',
    amount: '$415917.71',
    status: 'BMW',
  },
  {
    id: 15,
    date_initiated: '19/08/2020',
    merchant_name: 'Talane',
    currency: 'RUB',
    amount: '$1601240.24',
    status: 'Volkswagen',
  },
  {
    id: 16,
    date_initiated: '03/06/2021',
    merchant_name: 'Quimba',
    currency: 'CNY',
    amount: '$1088102.12',
    status: 'Toyota',
  },
  {
    id: 17,
    date_initiated: '01/09/2019',
    merchant_name: 'Mynte',
    currency: 'IDR',
    amount: '$4415048.11',
    status: 'Dodge',
  },
  {
    id: 18,
    date_initiated: '25/09/2020',
    merchant_name: 'Viva',
    currency: 'IDR',
    amount: '$8938956.67',
    status: 'Buick',
  },
  {
    id: 19,
    date_initiated: '30/01/2021',
    merchant_name: 'Edgeify',
    currency: 'USD',
    amount: '$7012555.36',
    status: 'Suzuki',
  },
  {
    id: 20,
    date_initiated: '15/07/2021',
    merchant_name: 'Edgepulse',
    currency: 'THB',
    amount: '$9652646.66',
    status: 'Chevrolet',
  },
  {
    id: 21,
    date_initiated: '22/07/2020',
    merchant_name: 'Edgeify',
    currency: 'SYP',
    amount: '$9799250.18',
    status: 'Ford',
  },
  {
    id: 22,
    date_initiated: '10/01/2020',
    merchant_name: 'Devpulse',
    currency: 'RUB',
    amount: '$4612044.05',
    status: 'Audi',
  },
  {
    id: 23,
    date_initiated: '17/08/2019',
    merchant_name: 'Meevee',
    currency: 'IDR',
    amount: '$9510966.36',
    status: 'Toyota',
  },
  {
    id: 24,
    date_initiated: '10/03/2021',
    merchant_name: 'Edgetag',
    currency: 'CNY',
    amount: '$7911865.02',
    status: 'Mercedes-Benz',
  },
  {
    id: 25,
    date_initiated: '31/12/2020',
    merchant_name: 'Divavu',
    currency: 'CNY',
    amount: '$9378547.58',
    status: 'Ford',
  },
  {
    id: 26,
    date_initiated: '17/08/2020',
    merchant_name: 'Oyoyo',
    currency: 'IDR',
    amount: '$5230619.70',
    status: 'Honda',
  },
  {
    id: 27,
    date_initiated: '23/06/2020',
    merchant_name: 'Linkbridge',
    currency: 'PHP',
    amount: '$5531999.37',
    status: 'Toyota',
  },
  {
    id: 28,
    date_initiated: '07/12/2019',
    merchant_name: 'Rhyloo',
    currency: 'PHP',
    amount: '$6198801.52',
    status: 'Honda',
  },
  {
    id: 29,
    date_initiated: '24/11/2019',
    merchant_name: 'Riffpedia',
    currency: 'XCD',
    amount: '$1543955.38',
    status: 'Acura',
  },
  {
    id: 30,
    date_initiated: '09/07/2019',
    merchant_name: 'Edgeblab',
    currency: 'THB',
    amount: '$3906209.42',
    status: 'Ford',
  },
  {
    id: 31,
    date_initiated: '21/07/2019',
    merchant_name: 'Riffpedia',
    currency: 'USD',
    amount: '$9502502.55',
    status: 'Ford',
  },
  {
    id: 32,
    date_initiated: '04/11/2020',
    merchant_name: 'Bubbletube',
    currency: 'AFN',
    amount: '$2667798.25',
    status: 'Ford',
  },
  {
    id: 33,
    date_initiated: '01/10/2019',
    merchant_name: 'Tavu',
    currency: 'CNY',
    amount: '$8877420.35',
    status: 'Dodge',
  },
  {
    id: 34,
    date_initiated: '14/03/2021',
    merchant_name: 'Oozz',
    currency: 'UYU',
    amount: '$8015096.72',
    status: 'Mercury',
  },
  {
    id: 35,
    date_initiated: '15/07/2020',
    merchant_name: 'Jatri',
    currency: 'IRR',
    amount: '$6742496.92',
    status: 'Mazda',
  },
  {
    id: 36,
    date_initiated: '19/03/2021',
    merchant_name: 'Wordpedia',
    currency: 'CNY',
    amount: '$4322167.59',
    status: 'Chrysler',
  },
  {
    id: 37,
    date_initiated: '07/07/2019',
    merchant_name: 'Fiveclub',
    currency: 'PLN',
    amount: '$2225881.93',
    status: 'Chevrolet',
  },
  {
    id: 38,
    date_initiated: '03/09/2019',
    merchant_name: 'Dynabox',
    currency: 'CNY',
    amount: '$8219404.75',
    status: 'Chevrolet',
  },
  {
    id: 39,
    date_initiated: '07/06/2021',
    merchant_name: 'Minyx',
    currency: 'XPF',
    amount: '$5611981.48',
    status: 'Chevrolet',
  },
  {
    id: 40,
    date_initiated: '13/01/2020',
    merchant_name: 'Ntag',
    currency: 'JPY',
    amount: '$5000858.77',
    status: 'Chevrolet',
  },
  {
    id: 41,
    date_initiated: '08/09/2019',
    merchant_name: 'Agivu',
    currency: 'EUR',
    amount: '$3876339.44',
    status: 'Mercury',
  },
  {
    id: 42,
    date_initiated: '10/07/2019',
    merchant_name: 'Chatterbridge',
    currency: 'IDR',
    amount: '$1825428.55',
    status: 'Dodge',
  },
  {
    id: 43,
    date_initiated: '17/11/2020',
    merchant_name: 'Yakitri',
    currency: 'EUR',
    amount: '$4023659.83',
    status: 'Chevrolet',
  },
  {
    id: 44,
    date_initiated: '17/12/2019',
    merchant_name: 'Fiveclub',
    currency: 'HNL',
    amount: '$3215565.10',
    status: 'GMC',
  },
  {
    id: 45,
    date_initiated: '22/09/2020',
    merchant_name: 'Yamia',
    currency: 'CNY',
    amount: '$6799284.39',
    status: 'Pontiac',
  },
  {
    id: 46,
    date_initiated: '02/01/2021',
    merchant_name: 'Flipstorm',
    currency: 'CNY',
    amount: '$1986274.22',
    status: 'Bentley',
  },
  {
    id: 47,
    date_initiated: '29/11/2020',
    merchant_name: 'Fiveclub',
    currency: 'CNY',
    amount: '$6373480.46',
    status: 'Buick',
  },
  {
    id: 48,
    date_initiated: '22/10/2019',
    merchant_name: 'Blogspan',
    currency: 'SEK',
    amount: '$3426127.72',
    status: 'Ford',
  },
  {
    id: 49,
    date_initiated: '21/02/2021',
    merchant_name: 'Flashset',
    currency: 'ZWL',
    amount: '$6262527.12',
    status: 'Hyundai',
  },
  {
    id: 50,
    date_initiated: '21/10/2020',
    merchant_name: 'Dabtype',
    currency: 'CNY',
    amount: '$8116351.65',
    status: 'Ford',
  },
];

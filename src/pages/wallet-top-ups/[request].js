import Image from 'next/image';
import { useRouter } from 'next/router';

import DashboardLayout from '../../components/Layout/DashboardLayout';
import { DASHBOARD } from '../../constants/routes';

export default function TopUpRequest() {
  const router = useRouter();

  return (
    <div>
      <div className="flex items-start">
        <Image
          src="/images/back-icon.svg"
          width={27}
          height={27}
          unoptimized
          alt="Click to go back"
          className="cursor-pointer"
          onClick={
            history.length > 2 ? router.back : () => router.push(DASHBOARD.WALLET_TOP_UPS)
          }
        />
        <h2 className="ml-2 text-2xl font-medium">Viewing Proof of Payment</h2>
      </div>

      <h3 className="mt-7 font-medium">Transaction Details</h3>
      <p className="mt-2 text-sm text-light">
        Here’s info the Merchant uploaded as proof of payment
      </p>

      <div className="mt-6 border border-white-200 rounded-[4px] shadow-header px-8 pb-10 pt-8">
        <div className="flex">
          {new Array(4).fill('yah').map((_, index) => (
            <div className="mr-24 last:mr-0" key={index}>
              <h4 className="mb-1 text-dark-100 text-sm">Merchant Name</h4>
              <p className="text-light text-sm whitespace-nowrap">Akintola & Company</p>
            </div>
          ))}
        </div>
      </div>

      <h3 className="mt-7 font-medium">Payment Details</h3>
      <p className="mt-2 text-sm text-light">
        Here’s info the Merchant uploaded as proof of payment
      </p>

      <div className="mt-6 border border-white-200 rounded-[4px] shadow-header px-8 pb-10 pt-8">
        <div className="flex">
          {new Array(4).fill('yah').map((_, index) => (
            <div className="mr-24 last:mr-0" key={index}>
              <h4 className="mb-1 text-dark-100 text-sm">Merchant Name</h4>
              <p className="text-light text-sm whitespace-nowrap">Akintola & Company</p>
            </div>
          ))}
        </div>
      </div>

      <h3 className="mt-7 font-medium">Documents Uploaded</h3>
      <p className="mt-2 text-sm text-light">
        Here are the documents Merchant uploaded for KYB
      </p>

      <div className="flex flex-wrap">
        {new Array(4).fill('yah').map((_, index) => (
          <div
            key={index}
            className="mt-6 mr-6 border border-white-200 w-max rounded-[4px] shadow-header px-4 py-6 flex items-center cursor-pointer"
          >
            <Image src="/images/sheet.svg" width={40} height={40} alt="" unoptimized />
            <div className="ml-4 mr-12">
              <h4 className="mb-1 text-dark-100 text-sm">Transfer of receipt.png</h4>
              <p className="text-light text-sm whitespace-nowrap">Document Uploaded</p>
            </div>
            <Image src="/images/download.svg" width={40} height={40} alt="" unoptimized />
          </div>
        ))}
      </div>
    </div>
  );
}

TopUpRequest.getLayout = page => <DashboardLayout>{page}</DashboardLayout>;

TopUpRequest.protected = true;

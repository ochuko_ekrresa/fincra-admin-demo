import React from 'react';
import { AuthLayout } from '../../components/Layout';
import LoginForm from '../../components/auth/login-form';

const Login = () => {
  return (
    <AuthLayout
      title="Admin Login."
      subtitle="Log in to your account."
      sideImage="https://images.unsplash.com/photo-1588612568467-a6b245a1f4a5?ixlib=rb-1.2.1&auto=format&fit=crop&w=1300&q=80"
    >
      <LoginForm />
    </AuthLayout>
  );
};

export default Login;

import Pusher from 'pusher-js';
import { createContext } from 'react';

export const PusherContext = createContext();

const PUSHER_CONFIG = process.env.pusher;

// const pusher = new Pusher(PUSHER_CONFIG.key, {
//   cluster: PUSHER_CONFIG.cluster
// });

export const PusherProvider = ({ children }) => {
  return <PusherContext.Provider value={null}>{children}</PusherContext.Provider>;
};

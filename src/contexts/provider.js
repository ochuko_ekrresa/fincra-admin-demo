import React from 'react';

import { PusherProvider } from './Pusher/pusher.context';

const Provider = ({ children }) => <PusherProvider>{children}</PusherProvider>;

export default Provider;

import crypto from 'crypto';
import localstorage from '../utils/localstorage';

export const currencyFormat = (amount, currency, showCurrency = true, precision) => {
  if (!amount && currency)
    return `${showCurrency ? String(currency).toUpperCase() : ''} 0`.trim();
  if (!amount && !currency) return 'N/A';
  if (!currency)
    return amount.toLocaleString('en-US', {
      maximumFractionDigits: 4,
    });
  const upperCasedCurrency = currency.toUpperCase();

  if (showCurrency)
    return `${String(upperCasedCurrency)} ${amount.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: precision || currencyPrecisionNumber(upperCasedCurrency),
    })}`;
  return amount.toLocaleString('en-US', {
    minimumFractionDigits: 2,
    maximumFractionDigits: precision || currencyPrecisionNumber(upperCasedCurrency),
  });
};

export const validateEmail = email => {
  const re =
    /^(([^<>()[\]\\.,;:!#$%^&*'"()/~`?\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const startAndEnd = str => {
  if (str?.length > 25) {
    return str.substr(0, 10) + '...' + str.substr(str.length - 10, str.length);
  }
  return str;
};

export const getFlagUrl = (strings, countryCode) => {
  return `https://www.countryflags.io/${countryCode}/flat/48.png`;
};

export const currencyPrecisionNumber = currency => {
  currency = String(currency).toUpperCase();
  if (currency == 'NGN') return 2;
  if (currency == 'XRP') return 6;
  return 8;
};

export const currencyObj = (currency, currencyArray) => {
  const currencySingleArray = currencyArray.filter(item => {
    return item.code == currency.toUpperCase();
  });
  return currencySingleArray[0];
};

export const dateFormat = (date, locale = 'en-US') => {
  return new Date(date).toLocaleString(locale);
};

export const getCurrencyAndName = (currency, adminCurrencies) => {
  if (currency && adminCurrencies) {
    return adminCurrencies.find(
      ({ code }) => code.toUpperCase() === currency.toUpperCase()
    );
  }
  return false;
};

export const getCurrenciesAdminAccepts = (currencyArray, adminCurrencies) => {
  const acceptedCurrencies = [];

  currencyArray.forEach(currency => {
    const currencyAccepted = getCurrencyAndName(currency, adminCurrencies);
    if (currencyAccepted) {
      acceptedCurrencies.push(currency);
    }
  });

  return acceptedCurrencies;
};

export const payoutOption = {
  bankOptions: {
    list() {
      return localstorage.getItem('business')?.settings?.bankOptions || [];
    },
    add(bankName, bankCode, accountNumber, currentThis = this) {
      return [...currentThis.list(), { bankName, bankCode, accountNumber }];
    },
    remove(id, currentThis = this) {
      return currentThis
        .list()
        .filter(account => {
          return account._id !== id;
        })
        .reverse();
    },
  },
  walletAddressesForPayout: {
    list() {
      return localstorage.getItem('business')?.settings?.walletAddressesForPayout || [];
    },
    add(currency, wallet, currentThis = this) {
      return [...currentThis.list(), { currency, wallet }];
    },
    remove(id, currentThis = this) {
      return currentThis
        .list()
        .filter(walletAddress => {
          return walletAddress._id !== id;
        })
        .reverse();
    },
  },
};

export function decrypt(ivData, encryptedData, encryptDecryptkey, isObject = true) {
  let iv = Buffer.from(ivData, 'hex');
  let encryptedText = Buffer.from(encryptedData, 'hex');
  let decipher = crypto.createDecipheriv(
    'aes-256-cbc',
    Buffer.from(JSON.parse(encryptDecryptkey)),
    iv
  );
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return isObject ? JSON.parse(decrypted.toString()) : decrypted.toString();
}

export const analysePassword = password => ({
  minLetters: new RegExp('^(?=.{8,})').test(password),
  containsUpperCase: new RegExp('^(?=.*[A-Z])').test(password),
  containsLowerCase: new RegExp('^(?=.*[a-z])').test(password),
  containsSymbols: new RegExp('^(?=.*[!@#$%^&(){}.,/\\-\\_+=<>*])').test(password),
  containsNumbers: new RegExp('^(?=.*[0-9])').test(password),
  containsAll: new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&(){}.,/\\-\\_+=<>*])(?=.{8,})'
  ).test(password),
  getMessage() {
    if (!this.minLetters) {
      return 'at least 8 characters';
    }
    if (!this.containsLowerCase) {
      return 'add lowercase';
    }
    if (!this.containsUpperCase) {
      return 'needs an uppercase';
    }
    if (!this.containsNumbers) {
      return 'just a number, at least';
    }
    if (!this.containsSymbols) {
      return 'please, add a special character';
    }
  },
});

export const getInitials = name => {
  const splitName = name?.split(' ');
  if (!name) {
    return '';
  } else if (splitName.length === 1) {
    return `${splitName[0][0]}${splitName[0][1]}`.toUpperCase();
  } else {
    return `${splitName[0][0]}${splitName[1][0]}`.toUpperCase();
  }
};

export const capitalizeFirstLetter = string => {
  return string && string[0].toUpperCase() + string.slice(1);
};

export const capitalizeWords = (string = '', delimiter = ' ') => {
  let splitStrings = string.split(delimiter);
  return splitStrings.map(string => capitalizeFirstLetter(string)).join(' ');
};

export const camelCaseToWords = (string, allCapsLength = 4) => {
  const splitWords = string.replace(/([a-z])([A-Z])/, '$1 $2');
  return splitWords.length > allCapsLength
    ? capitalizeWords(splitWords)
    : splitWords.toUpperCase();
};

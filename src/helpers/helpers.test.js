import {
  analysePassword,
  currencyFormat,
  currencyObj,
  currencyPrecisionNumber,
  dateFormat,
  decrypt,
  getCurrenciesAdminAccepts,
  getCurrencyAndName,
  getFlagUrl,
  getInitials,
  payoutOption,
  startAndEnd,
  validateEmail,
} from './';

describe('Analyse Password', () => {
  const minimumLettersPasswords = ['emmanuel', 'ayodelewole', 'foundation'];
  const upperCasePasswords = ['EMMA', 'eMmaNuel', 'john123K'];
  const lowerCasePasswords = ['najks', 'iuuihayiusbs', 'kubausbkuah'];
  const numericPasswords = ['ihbas12', '1821872', 'l8yhw,bs,dhjb889'];
  const symbolicPasswords = ['iady@763', 'hs.ksd', 'l8yhw,bs-,9'];
  const validPasswords = ['@joHnThomas10', 'Iranwobaby123_', 'HarleyQuinn123_'];
  const invalidPasswords = [
    'pass',
    'joHnThomas10',
    'iranwobaby123_',
    'password',
    'Password_',
    '_PASSWORD123',
  ];

  it('should test only minimum letters', () => {
    expect(minimumLettersPasswords.length).toBeGreaterThan(0);

    minimumLettersPasswords.forEach(password => {
      expect(analysePassword(password).minLetters).toBeTruthy();
    });
  });

  it('should test only uppercase', () => {
    expect(upperCasePasswords.length).toBeGreaterThan(0);

    upperCasePasswords.forEach(password => {
      expect(analysePassword(password).containsUpperCase).toBeTruthy();
    });
  });

  it('should test only lowercase', () => {
    expect(lowerCasePasswords.length).toBeGreaterThan(0);

    lowerCasePasswords.forEach(password => {
      expect(analysePassword(password).containsLowerCase).toBeTruthy();
    });
  });

  it('should test only symbols', () => {
    expect(symbolicPasswords.length).toBeGreaterThan(0);

    symbolicPasswords.forEach(password => {
      expect(analysePassword(password).containsSymbols).toBeTruthy();
    });
  });

  it('should test only numbers', () => {
    expect(numericPasswords.length).toBeGreaterThan(0);

    numericPasswords.forEach(password => {
      expect(analysePassword(password).containsNumbers).toBeTruthy();
    });
  });

  test('should test password and return message correctly', () => {
    validPasswords.forEach(password => {
      expect(analysePassword(password).containsAll).toBeTruthy();
      expect(analysePassword(password).getMessage()).toBeUndefined();
    });

    invalidPasswords.forEach(password => {
      const message = analysePassword(password).getMessage();
      expect(analysePassword(password).containsAll).toBeFalsy();
      expect(message).not.toBeUndefined;
    });
  });
});

describe('Currency Formatter', () => {
  it('should not show currency when specified', () => {
    expect(currencyFormat(1000, 'ngn', false)).toBe('1,000.00');
  });

  test('currency is case insensitive', () => {
    const expected = currencyFormat(0.0104892, 'btc');
    expect(expected).toBe('BTC 0.0104892');
  });

  it('should not exceed precision limit', () => {
    expect(currencyFormat(1.21859, 'btc')).toBe('BTC 1.21859');
    expect(currencyFormat(0.010489200129, 'btc')).toBe('BTC 0.0104892');
    expect(currencyFormat(0.010489210129, 'btc')).toBe('BTC 0.01048921');

    expect(currencyFormat(1.21859, 'xrp')).toBe('XRP 1.21859');
    expect(currencyFormat(0.010489200129, 'xrp')).toBe('XRP 0.010489');

    expect(currencyFormat(211.21859, 'ngn')).toBe('NGN 211.22');
    expect(currencyFormat(1000, 'ngn')).toBe('NGN 1,000.00');
  });

  it('should round up decimals', () => {
    expect(currencyFormat(384.92859, 'ngn')).toBe('NGN 384.93');
    expect(currencyFormat(0.010489267129, 'eth')).toBe('ETH 0.01048927');
  });

  it('should add comma to amount where necessary', () => {
    expect(currencyFormat(10000, 'ngn')).toBe('NGN 10,000.00');
    expect(currencyFormat(1823839.93, 'ngn')).toBe('NGN 1,823,839.93');
  });

  it('should be able to manually override precision', () => {
    expect(currencyFormat(211.21859, 'ngn', true, 4)).toBe('NGN 211.2186');
  });

  it("should return N/A if there's no amount and currency", () => {
    expect(currencyFormat('', '')).toBe('N/A');
    expect(currencyFormat(null, null)).toBe('N/A');
  });

  it("should not return more than 4 decimal places if there's no currency", () => {
    expect.assertions(3);

    expect(currencyFormat(1000)).toBe('1,000');
    expect(currencyFormat(1000.12)).toBe('1,000.12');
    expect(currencyFormat(1000.123456)).toBe('1,000.1235');
  });

  it('should handle falsy amount', () => {
    expect(currencyFormat(null, 'ngn')).toBe('NGN 0');
    expect(currencyFormat(undefined, 'btc')).toBe('BTC 0');
    expect(currencyFormat(false, 'ngn', false)).toBe('0');
  });
});

describe('Currency Object', () => {
  const currencies = [
    {
      name: 'Bitcoin',
      code: 'BTC',
      currencyType: 'crypto',
    },
    {
      name: 'Litecoin',
      code: 'LTC',
      currencyType: 'crypto',
    },
  ];
  it('should return a currency object if currency exists', () => {
    expect(currencyObj('ltc', currencies)).toEqual({
      name: 'Litecoin',
      code: 'LTC',
      currencyType: 'crypto',
    });
  });

  it('should return undefined if currency does not exist', () => {
    expect(currencyObj('jpy', currencies)).toBe(undefined);
  });
});

describe('Currency Precision Number', () => {
  it('should be case-insensitive', () => {
    expect(currencyPrecisionNumber('ngn')).toBe(2);
    expect(currencyPrecisionNumber('NGN')).toBe(2);
  });

  test('NGN should return 2', () => {
    expect(currencyPrecisionNumber('ngn')).toBe(2);
  });

  test('XRP should return 6', () => {
    expect(currencyPrecisionNumber('xrp')).toBe(6);
  });

  test('Other currencies should return 8', () => {
    expect(currencyPrecisionNumber('eth')).toBe(8);
  });
});

describe('Date Format', () => {
  it('should format properly in the specified locale', () => {
    expect(dateFormat('05-02-20 10:21:43', 'en-US')).toBe('5/2/2020, 10:21:43 AM');
    // expect(dateFormat("05/02/20 10:21:43", "de-AT")).toBe("5/2/2020, 10:21:43");
    expect(dateFormat('05-02/20 10:21:43', 'en-GB')).toBe('02/05/2020, 10:21:43');
    expect(dateFormat('Tue May 19 2020 06:42:09 GMT+0100', 'en-NG')).toBe(
      '19/05/2020, 06:42:09'
    );
  });

  it('should handle invalid date', () => {
    expect(dateFormat('lol')).toBe('Invalid Date');
    expect(dateFormat('')).toBe('Invalid Date');
  });
});

describe('Decrypt', () => {});

describe('Get Currency Admin Accepts', () => {
  const adminCurrencies = [
    {
      name: 'Bitcoin',
      code: 'BTC',
      currencyType: 'crypto',
    },
    {
      name: 'Litecoin',
      code: 'LTC',
      currencyType: 'crypto',
    },
  ];

  const currencies = ['btc', 'eth', 'ltc'];

  it('should return currency currencies that admin accepts', () => {
    const expected = getCurrenciesAdminAccepts(currencies, adminCurrencies);

    expect(expected).toEqual(['btc', 'ltc']);
  });

  it("should return false is there's neither currency nor adminCurrencies", () => {
    expect.assertions(3);

    expect(getCurrencyAndName('', adminCurrencies)).toBe(false);
    expect(getCurrencyAndName('btc')).toBe(false);
    expect(getCurrencyAndName()).toBe(false);
  });
});

describe('Get Currency and Name', () => {
  const adminCurrencies = [
    {
      name: 'Bitcoin',
      code: 'BTC',
      minimumSend: 0.0002,
      disbursementFee: { structure: 'percentage', value: 1 },
      collectionFee: { structure: 'percentage', value: 0 },
      currencyType: 'crypto',
    },
    {
      name: 'Litecoin',
      code: 'LTC',
      minimumSend: 0.004,
      disbursementFee: { structure: 'percentage', value: 1 },
      collectionFee: { structure: 'percentage', value: 0 },
      currencyType: 'crypto',
    },
    {
      name: 'Tether',
      code: 'USDT',
      minimumSend: 2,
      disbursementFee: { structure: 'percentage', value: 1 },
      collectionFee: { structure: 'percentage', value: 0 },
      currencyType: 'crypto',
    },
  ];

  it('should return currency and name', () => {
    const expected = getCurrencyAndName('USDT', adminCurrencies);

    expect(expected).toEqual({
      name: 'Tether',
      code: 'USDT',
      minimumSend: 2,
      disbursementFee: { structure: 'percentage', value: 1 },
      collectionFee: { structure: 'percentage', value: 0 },
      currencyType: 'crypto',
    });
  });

  it("should return false is there's neither currency nor adminCurrencies", () => {
    expect.assertions(3);

    expect(getCurrencyAndName('', adminCurrencies)).toBe(false);
    expect(getCurrencyAndName('btc')).toBe(false);
    expect(getCurrencyAndName()).toBe(false);
  });
});

describe('Get Flag URL', () => {
  const countries = ['ng', 'us', 'uk', 'az'];

  it('should return the correct URL', () => {
    const expected = countries.every(
      country =>
        getFlagUrl`${country}` === `https://www.countryflags.io/${country}/flat/48.png`
    );

    expect(expected).toBe(true);
  });
});

describe('Get Initials', () => {
  it('works correctly', () => {
    expect(getInitials('')).toBe('');
    expect(getInitials('Fliqpay')).toBe('FL');
    expect(getInitials('Somto Enterprise')).toBe('SE');
    expect(getInitials('Somto Enterprise LTD')).toBe('SE');
  });
});

describe('Payout Option', () => {
  describe('Bank Account', () => {
    let data = [
      {
        _id: '1',
        bankName: 'ZENITH BANK PLC',
        bankCode: '057',
        accountNumber: '2150309716',
      },
    ];
    let bankOptions;

    beforeEach(() => {
      const { add, remove } = payoutOption.bankOptions;

      const list = jest.fn().mockReturnValue(data);
      bankOptions = { add, remove, list };
    });

    it('should add a bank account', () => {
      const { add } = bankOptions;
      data = add('Kuda Bank', '032', '0231271289', bankOptions);
      expect(data).toEqual([
        {
          _id: '1',
          bankName: 'ZENITH BANK PLC',
          bankCode: '057',
          accountNumber: '2150309716',
        },
        {
          bankName: 'Kuda Bank',
          bankCode: '032',
          accountNumber: '0231271289',
        },
      ]);
    });

    it('should remove a bank account by id', () => {
      const { remove } = bankOptions;

      data = remove('1', bankOptions);
      expect(data).toEqual([
        {
          bankName: 'Kuda Bank',
          bankCode: '032',
          accountNumber: '0231271289',
        },
      ]);
    });
  });

  describe('Wallet Address', () => {
    let data = [
      {
        _id: '1',
        currency: 'BTC',
        wallet: '3HHmjP4R3mPANnFXSmp2UKRxrhmNWi5u1P',
      },
    ];
    let walletAddressesForPayout;

    beforeEach(() => {
      const { add, remove } = payoutOption.walletAddressesForPayout;

      const list = jest.fn().mockReturnValue(data);
      walletAddressesForPayout = { add, remove, list };
    });

    it('should add a bank account', () => {
      const { add } = walletAddressesForPayout;
      data = add('ETH', '0x2Na873mPANnFXSmp2UKrxhsmNWi5X0JQ', walletAddressesForPayout);
      expect(data).toEqual([
        {
          _id: '1',
          currency: 'BTC',
          wallet: '3HHmjP4R3mPANnFXSmp2UKRxrhmNWi5u1P',
        },
        {
          currency: 'ETH',
          wallet: '0x2Na873mPANnFXSmp2UKrxhsmNWi5X0JQ',
        },
      ]);
    });

    it('should remove a bank account by id', () => {
      const { remove } = walletAddressesForPayout;

      data = remove('1', walletAddressesForPayout);
      expect(data).toEqual([
        {
          currency: 'ETH',
          wallet: '0x2Na873mPANnFXSmp2UKrxhsmNWi5X0JQ',
        },
      ]);
    });
  });
});

describe('Start and End (Truncate middle string)', () => {
  it('should truncate correctly if input has > 25 characters', () => {
    const expected = startAndEnd('3HHmjP4R3mPANnFXSmp2UKRxrhmNWi5u1P');
    expect(expected).toBe('3HHmjP4R3m...rhmNWi5u1P');
  });

  it('should not truncate if input has <= 25 characters', () => {
    expect(startAndEnd('Hello Fliqqer!')).toBe('Hello Fliqqer!');
  });
});

describe('Validate Email', () => {
  const validEmails = [
    'olaayo10@gmail.com',
    'wole@fliqpay.com',
    'jhsdh@oisndnjs.com',
    'ui988_192@gmail.com',
    'iranwofoundation@yahoo.com',
    'buchi.okoro123@quidax.com',
  ];

  const invalidEmails = [
    'john@.com',
    'tems@gmail',
    'tunbosun*@quidax.com',
    '',
    'jargons.com',
    'plain text',
    'hello @gmail.com',
    'buchi.okoro123.@quidax.com',
  ];

  it('should validate emails correctly', () => {
    expect(validEmails.every(email => validateEmail(email))).toBe(true);
    expect(invalidEmails.every(email => validateEmail(email))).toBe(false);
  });
});

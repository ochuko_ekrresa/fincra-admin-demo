import { render } from '@testing-library/react';
import LoginPage from '../../pages/auth/login';

jest.mock('../../components/auth/login-form', () => () => 'LoginForm');

describe('Login Page', () => {
  it('should render login form', () => {
    const { container } = render(<LoginPage />);
    expect(container).toMatchInlineSnapshot(`
      <div>
        <div
          class="h-full flex flex-col"
        >
          <nav
            class="flex items-center w-full p-6 h-20 bg-white sticky top-0 z-10"
          >
            <img
              alt="Fliqpay"
              class="h-8 cursor-pointer"
              src="/images/logo-primary.svg"
            />
          </nav>
          <div
            class="flex justify-center w-full flex-1 md:w-3/5 mt-6 md:mt-12"
          >
            <div
              class="w-full bg-white flex flex-col p-6 md:p-0 items-center text-sm text-primarydark pb-8"
            >
              <div
                class="flex flex-col w-full md:w-sm"
              >
                <div
                  class="flex flex-col items-center my-8 font-medium text-center"
                >
                  <h1
                    class="text-3xl md:text-4xl text-secondary mb-1"
                  >
                    Admin Login.
                  </h1>
                  <p
                    class="text-base font-normal text-primarydark my-1"
                  >
                    Log in to your account.
                  </p>
                </div>
                <div
                  class="w-full"
                >
                  LoginForm
                </div>
              </div>
            </div>
            <div
              class="w-2/5 bg-blue-100 h-screen fixed right-0 top-0 z-20 hidden md:block"
            >
              <img
                class="object-cover h-full w-full"
                src="https://images.unsplash.com/photo-1588612568467-a6b245a1f4a5?ixlib=rb-1.2.1&auto=format&fit=crop&w=1300&q=80"
              />
            </div>
          </div>
        </div>
      </div>
    `);
  });
});

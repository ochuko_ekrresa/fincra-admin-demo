import Logout from '../../pages/auth/logout';
import { render } from '@testing-library/react';

jest.mock('../../utils/api');

describe('Logout', () => {
  it('should render nothing', () => {
    const { container } = render(<Logout />);
    expect(container).toBeEmpty();
  });
});

import { render } from '@testing-library/react';
import App from '../pages/_app';

describe('App component', () => {
  it('should render correctly', () => {
    expect(new App()).toMatchInlineSnapshot(`
      FliqPayApp {
        "context": undefined,
        "props": undefined,
        "refs": Object {},
        "updater": Object {
          "enqueueForceUpdate": [Function],
          "enqueueReplaceState": [Function],
          "enqueueSetState": [Function],
          "isMounted": [Function],
        },
      }
    `);
  });
});

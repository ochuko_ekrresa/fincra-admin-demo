import Document from '../pages/_document';

describe('Document component', () => {
  it('should render correctly', () => {
    expect(new Document()).toMatchInlineSnapshot(`
      MyDocument {
        "context": undefined,
        "props": undefined,
        "refs": Object {},
        "updater": Object {
          "enqueueForceUpdate": [Function],
          "enqueueReplaceState": [Function],
          "enqueueSetState": [Function],
          "isMounted": [Function],
        },
      }
    `);
  });
});

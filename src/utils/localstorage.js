const setItem = (name, data) =>
  localStorage.setItem(name, typeof data === 'object' ? JSON.stringify(data) : data);

const getItem = name => {
  const data = window.localStorage.getItem(name);
  try {
    return JSON.parse(data);
  } catch {
    return data;
  }
};

const removeItem = name => localStorage.removeItem(name);

const removeItems = (...items) => items.map(item => localStorage.removeItem(item));

const clearStorage = () => localStorage.clear();

export default { setItem, getItem, removeItem, removeItems, clearStorage };

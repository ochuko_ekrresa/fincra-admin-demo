import logrocket from 'logrocket';
import mixpanel from 'mixpanel-browser';

const identify = user => {
  if (process.env.NODE_ENV === 'production') {
    logrocket.identify(user.email, user);
    window.analytics.identify(user._id, user);

    logrocket.getSessionURL(sessionURL => {
      window.analytics.track('LogRocket Session', { sessionURL });
    });
  }
  if (process.env.NODE_ENV === 'test') {
    logrocket.identify(user.email, user);
  }
};

const updateProfile = data => {
  mixpanel.people.set(data);
};

const track = (event, additionalData) => {
  if (process.env.NODE_ENV === 'production') {
    window.analytics.track(event, additionalData);
  }
};

const analytics = { identify, updateProfile, track };

export default analytics;

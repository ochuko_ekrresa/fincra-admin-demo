import localstorage from './localstorage';

describe('Local Storage', () => {
  const items = [
    { name: 'user', data: { name: 'John Wick', email: 'ijohnwick@yahoo.com' } },
    { name: 'business_name', data: 'Pizza Republic' },
    { name: 'fuel_price', data: 125 },
  ];

  const getItem = jest.fn().mockImplementation(name => localstorage.getItem(name));

  beforeAll(() => {
    //   Set all items in localstorage
    items.forEach(item => {
      localstorage.setItem(item.name, item.data);
    });
  });

  it('should set and get an item', () => {
    expect.assertions(3);
    items.forEach(({ name, data }) => {
      const storedData = getItem(name);
      expect(storedData).toEqual(data);
    });
  });

  it('should set and get an object type', () => {
    const { name, data } = items[0];
    const storedData = getItem(name);

    expect.assertions(3);

    expect(typeof data).toBe('object');
    expect(typeof storedData).toBe('object');
    expect(storedData).toEqual(data);
  });

  it('should be called with the right data', () => {
    expect(getItem).toHaveBeenCalledWith('business_name');
  });

  it('should remove an item', () => {
    const itemName = 'fuel_price';
    localstorage.removeItem(itemName);

    const storedData = getItem(itemName);
    expect(storedData).toBe(null);
  });

  it('should remove multiple items', () => {
    const items = [
      { name: 'company', data: 'fliqpay' },
      { name: 'age', data: 'a year' },
      { name: 'random', data: 'pay tithes' },
    ];

    // Set items
    items.forEach(({ name, data }) => {
      localstorage.setItem(name, data);
    });

    // Check that items were stored
    items.forEach(({ name, data }) => {
      const storedData = getItem(name);
      expect(storedData).toEqual(data);
    });

    // Remove items
    localstorage.removeItems('company', 'age');

    // Check that items have been removed
    items
      .filter(({ name }) => name !== 'random')
      .forEach(({ name }) => {
        const storedData = getItem(name);
        expect(storedData).toBe(null);
      });

    // Check that the last item wasn't removed
    expect(getItem('random')).toEqual('pay tithes');
  });

  it('should clear storage', () => {
    localstorage.clearStorage();

    items.forEach(({ name }) => {
      const storedData = getItem(name);
      expect(storedData).toBe(null);
    });
  });
});

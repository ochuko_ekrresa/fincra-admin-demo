import React from 'react';
import toaster from 'toasted-notes';
import { Alert } from '../components';

export default function Toast({
  title,
  message,
  type,
  size = 'md',
  className,
  position = 'top-right',
  duration,
  ...otherProps
}) {
  toaster.notify(
    ({ onClose }) => (
      <Alert
        title={title}
        type={type}
        size={size}
        className={`my-2 ${className} absolute top-0`}
        message={message}
        {...otherProps}
      />
    ),
    {
      duration: duration || 2500,
      position,
    }
  );
}

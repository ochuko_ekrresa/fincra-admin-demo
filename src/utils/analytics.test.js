import analytics from './analytics';
import logrocket from 'logrocket';
import mixpanel from 'mixpanel-browser';

jest.mock('mixpanel-browser');
jest.mock('logrocket');

logrocket.identify = jest.fn();
mixpanel.people = { set: jest.fn() };

describe('Analytics', () => {
  it('should identify user', () => {
    analytics.identify('test_user');
    expect(logrocket.identify).toHaveBeenCalled();
  });

  it('should update profile', () => {
    analytics.updateProfile({ name: 'emma' });
    expect(mixpanel.people.set).toHaveBeenCalled();
  });
});

import { useEffect } from 'react';
import Router from 'next/router';
import LocalStorage from '../localstorage';
import API from './api';
import * as ROUTES from '../../constants/routes';

const checkIfUserDoesNotExist = async error => {
  const user = LocalStorage.getItem('user');

  try {
    // API.getUser(user._id, undefined, error);
    // await API.getSettings(() => {}, error);
  } catch {
    error();
  }
};

const withAuthSync = WrappedComponent => {
  const Wrapper = props => {
    const syncEffect = event => {
      if (event.key === 'logout') {
        console.log('logged out from storage!');
        Router.push(ROUTES.AUTH.LOGOUT);
      }

      if (event.key === 'user') {
        checkIfUserDoesNotExist(() => {
          LocalStorage.removeItem('user');
          Router.push(ROUTES.AUTH.LOGOUT);
        });
      }
    };

    useEffect(() => {
      checkIfUserDoesNotExist(() => Router.push(ROUTES.AUTH.LOGOUT));

      window.addEventListener('storage', syncEffect);

      return () => {
        window.removeEventListener('storage', syncEffect);
        window.localStorage.removeItem('logout');
      };
    }, []);

    return <WrappedComponent {...props} />;
  };

  Wrapper.getInitialProps = async ctx => {
    const token = API.auth(ctx);
    const componentProps =
      WrappedComponent.getInitialProps && (await WrappedComponent.getInitialProps(ctx));

    return { ...componentProps, token };
  };

  return Wrapper;
};

export default withAuthSync;

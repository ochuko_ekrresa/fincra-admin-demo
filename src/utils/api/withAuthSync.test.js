import React from 'react';
import WithAuthSync from './withAuthSync';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom/extend-expect';

describe('WithAuthSync', () => {
  it('renders correctly', () => {
    const El = WithAuthSync('Hello');
    const container = shallow(<El />);
    expect(container).toMatchInlineSnapshot('ShallowWrapper {}');
  });
});

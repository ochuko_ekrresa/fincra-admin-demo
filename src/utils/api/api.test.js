import API from './api';
import mockAxios from 'jest-mock-axios';
import cookie from 'js-cookie';
import Router from 'next/router';
import LogRocket from 'logrocket';
import analytics from '../analytics';
import localstorage from '../localstorage';

import { commonTests } from './api.test.data';

const mockSuccessResponse = { data: { success: true, data: { _id: '' } } };
const mockErrorResponse = {
  response: { data: { success: false, message: 'error', data: {} } },
};

jest.mock('next/router', () => ({ push: jest.fn() }));
jest.mock('logrocket', () => ({ captureException: jest.fn() }));
jest.mock('js-cookie', () => ({
  get: jest.fn(),
  set: jest.fn(),
  remove: jest.fn(),
}));
jest.mock('../localstorage', () => ({
  getItem: jest.fn().mockReturnValue({ _id: 'o8gy3kug2y3gyu' }),
  setItem: jest.fn(),
  removeItems: jest.fn(),
  clearStorage: jest.fn(),
}));

describe('API', () => {
  let lastRequest, successMock, errorMock;

  beforeEach(() => {
    successMock = jest.fn();
    errorMock = jest.fn();
  });

  afterEach(() => {
    // cleaning up the mess left behind the previous test
    mockAxios.reset();

    localstorage.setItem.mockClear();
    Router.push.mockClear();
  });

  describe('sign out', () => {
    it('should work correctly', () => {
      API.signOut();
      expect(cookie.remove).toHaveBeenCalled();
      expect(localstorage.setItem).toHaveBeenCalled();
      expect(localstorage.removeItems).toHaveBeenCalled();
      expect(Router.push).toHaveBeenCalled();
    });
  });

  Object.keys(commonTests).forEach(test => {
    const dependencies = commonTests[test].dependencies || [];
    const noPayload = commonTests[test].noPayload;
    const responseData = commonTests[test].responseData || {};

    describe(test, () => {
      beforeEach(() => {
        if (noPayload) {
          API[test](successMock, errorMock);
        } else {
          API[test]({}, successMock, errorMock);
        }

        lastRequest = mockAxios.lastReqGet();
      });

      if (dependencies.includes('success') || dependencies.includes('default')) {
        it('should call success callback', () => {
          const newMockSuccessResponse = {
            ...mockSuccessResponse,
            data: { ...mockSuccessResponse.data, data: responseData },
          };
          mockAxios.mockResponse(newMockSuccessResponse, lastRequest);
          expect(successMock).toHaveBeenCalled();
        });
      }

      if (dependencies.includes('localstorage')) {
        it('should set localstorage', () => {
          mockAxios.mockResponse(mockSuccessResponse, lastRequest);
          expect(localstorage.setItem).toHaveBeenCalled();
        });
      }

      if (dependencies.includes('router')) {
        it('should change route', () => {
          mockAxios.mockResponse(mockSuccessResponse, lastRequest);
          expect(Router.push).toHaveBeenCalled();
        });
      }

      if (dependencies.includes('set-cookie')) {
        it('should set cookie', () => {
          mockAxios.mockResponse(mockSuccessResponse, lastRequest);
          expect(cookie.set).toHaveBeenCalled();
        });
      }

      if (dependencies.includes('remove-cookie')) {
        it('should remove cookie', () => {
          mockAxios.mockResponse(mockSuccessResponse, lastRequest);
          expect(cookie.remove).toHaveBeenCalled();
        });
      }

      if (dependencies.includes('error') || dependencies.includes('default')) {
        it('should return error callback if request failed', () => {
          mockAxios.mockError(mockErrorResponse, lastRequest);
          expect(errorMock).toHaveBeenCalledWith(mockErrorResponse.response.data);
        });
      }

      if (dependencies.includes('logrocket')) {
        it('should capture exception on LogRocket', () => {
          mockAxios.mockError(mockErrorResponse, lastRequest);

          expect(LogRocket.captureException).toHaveBeenCalled();
        });
      }
    });
  });
});

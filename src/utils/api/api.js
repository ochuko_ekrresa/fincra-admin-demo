import LogRocket from 'logrocket';
import Axios from 'axios';
import Router from 'next/router';
import nextCookie from 'next-cookies';
import cookie from 'js-cookie';
import LocalStorage from '../localstorage';

import * as ROUTES from '../../constants/routes';
import {
  currencies,
  disbursements,
  merchants,
  payment_links,
  transaction_types,
  wallets,
} from '../../mock/mock.data';

const getBaseUrl = serviceName => {
  return `${process.env.backendURL}/${serviceName}`;
};

const setBearerToken = () => {
  Axios.defaults.headers.common = {
    Authorization: `Bearer ${cookie.get('token')}`,
    'api-key': process.env.apiKey,
  };
};

setBearerToken();

const API = {
  auth(ctx) {
    const { token } = nextCookie(ctx);
    // If there's no token, it means the user is not logged in.
    if (!token || token === 'undefined') {
      if (typeof window === 'undefined') {
        ctx.res.writeHead(302, { Location: ROUTES.AUTH.LOGIN });
        ctx.res.end();
      } else {
        Router.push(ROUTES.AUTH.LOGIN);
      }
    }

    return token;
  },

  async signIn({ email, password }, success = () => {}, error = () => {}) {
    const url = getBaseUrl('profile');

    await Axios.post(`${url}/auth/login`, { email, password })
      .then(response => {
        const { token, user } = response.data.data;
        cookie.set('token', token, { expires: 1 });

        setBearerToken();

        LocalStorage.removeItems('user');
        LocalStorage.setItem('user', user);

        Router.push(ROUTES.DASHBOARD.HOME);

        success(response?.data);
      })
      .catch(err => {
        LogRocket.captureException(new Error(err?.response?.data?.error));
        error(err?.response?.data || err);
      });
  },

  signOut() {
    cookie.remove('token');
    // to support logging out from all windows
    LocalStorage.setItem('logout', Date.now());

    LocalStorage.removeItems('user');
    Router.push(ROUTES.AUTH.LOGIN);
  },

  async getUser(id, success = () => {}, error = () => {}) {
    const url = getBaseUrl('profile');

    await Axios.get(`${url}/auth/${id}`)
      .then(response => {
        if (response.data.success) {
          success(response.data.data);
        }
      })
      .catch(err => {
        error(err?.response?.data || err);
      });
  },

  async getBusinesses(success, error) {
    const url = getBaseUrl('profile');

    await Axios.get(`${url}/business`)
      .then(res => {
        const businesses = res.data.data;
        success(businesses);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getBusinessById(id, success, error) {
    // TODO: fetch by ID
    const url = getBaseUrl('profile');

    await Axios.get(`${url}/business/${id}`).then(res => {
      const business = res.data.data;
      success(business);
    });
    // .catch((err) => error(err?.response?.data));
    // this.getBusinesses((data) => {
    //   console.log(data);
    //   success(data.find((business) => business._id === id));
    // });

    // success(businesses[0]);
  },
  async createCorridorStructure(data, success, error) {
    const url = getBaseUrl('corridorManagement');

    await Axios.post(`${url}/corridors/admin`, data)
      .then(res => {
        const corridors = res.data;
        success(corridors);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async updateCorridorStructure(id, data, success, error) {
    const url = getBaseUrl('corridorManagement');

    await Axios.patch(`${url}/corridors/admin/${id}`, data)
      .then(res => {
        const corridors = res.data;
        success(corridors);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getCorridors(success, error) {
    const url = getBaseUrl('corridorManagement');

    await Axios.get(`${url}/corridors/admin`)
      .then(res => {
        const corridors = res.data.data;
        success(corridors);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getCorridorById(id, success, error) {
    const url = getBaseUrl('corridorManagement');

    await Axios.get(`${url}/corridors/admin/${id}`)
      .then(res => {
        const channels = res.data.data;
        success(channels);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getChannels(success, error) {
    const url = getBaseUrl('core');

    await Axios.get(`${url}/channels`)
      .then(res => {
        const channels = res.data.data;
        success(channels);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getCurrencies(success, error) {
    success(currencies);
  },

  async getTransactionTypes(success, error) {
    success(transaction_types);
  },

  async getMerchants(success, error) {
    const data = merchants;
    success(data);
  },

  async getMerchantById(id, success, error) {
    const data = merchants[0];
    success(data);
  },

  async getPaymentLinks(success, error) {
    const data = payment_links;
    success(data);
  },

  async getPaymentLinkById(id, success, error) {
    const data = payment_links[0];
    success(data);
  },

  async getCollections(success, error) {
    const url = getBaseUrl('collections');

    await Axios.get(`${url}/collections`)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getCollectionById(id, success, error) {
    const data = [];
    success(data);
  },

  async getDisbursements(success, error) {
    const url = getBaseUrl('disbursements');

    await Axios.get(`${url}/disbursements`)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getDisbursementById(id, success, error) {
    const data = disbursements[0];
    success(data);
  },

  async getWallets(success, error) {
    const data = wallets;
    success(data);
  },

  async getExchangeRates(success, error) {
    const url = getBaseUrl('quotes');

    await Axios.get(`${url}/exchange-rates`)
      .then(res => {
        const rates = res.data.data;
        success(rates);
      })
      .catch(err => error && error(err?.response?.data));
  },
  async createExchangeRate(data, success, error) {
    const url = getBaseUrl('quotes');

    await Axios.post(`${url}/exchange-rates`, data)
      .then(res => {
        const rates = res.data.data;
        success(rates);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async updateExchangeRate(id, data, success, error) {
    const url = getBaseUrl('quotes');

    await Axios.patch(`${url}/exchange-rates/${id}`, data)
      .then(res => {
        const rates = res.data;
        success(rates);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async updateSettings(data, success, error) {
    const url = getBaseUrl('quotes');

    await Axios.patch(`${url}/admin-settings`, data)
      .then(res => {
        const settings = res.data.data;
        success(settings);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getSettings(success, error) {
    const url = getBaseUrl('quotes');

    await Axios.get(`${url}/admin-settings`)
      .then(res => {
        const settings = res.data.data;
        success(settings);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async retryDisbursement(data, success, error) {
    const url = getBaseUrl('disbursements');

    await Axios.post(`${url}/disbursements/retry`, data)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async succeedAndSubFunds(data, success, error) {
    const url = getBaseUrl('disbursements');

    await Axios.post(`${url}/accounting/succeed-and-sub-funds`, data)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async rejectAndUnlockFunds(data, success, error) {
    const url = getBaseUrl('disbursements');

    await Axios.post(`${url}/accounting/reject-and-unlock-funds`, data)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getQuotes(success, error) {
    success([
      {
        sourceCurrency: 'USD',
        destinationCurrency: 'NGN',
        rate: 480,
        transactionType: 'conversion',
        amount: 1000,
        quotedAmount: 480000,
        action: 'send',
      },
    ]);
  },

  async createVolatilityQuotient(data, success, error) {
    const url = getBaseUrl('quotes');

    await Axios.post(`${url}/volatility-quotient`, data)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async updateVolatilityQuotient(id, data, success, error) {
    const url = getBaseUrl('quotes');

    await Axios.patch(`${url}/volatility-quotient/${id}`, data)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getVolatilityQuotients(success, error) {
    const url = getBaseUrl('quotes');

    await Axios.get(`${url}/volatility-quotient?page=1&limit=100`)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async createSettlementConfig(data, success, error) {
    const url = getBaseUrl('core');

    await Axios.post(`${url}/settlement-config`, data)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getSettlementConfigs(success, error) {
    const url = getBaseUrl('core');

    await Axios.get(`${url}/settlement-config?page=1&limit=10000`)
      .then(res => {
        success(res.data.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getDefaultSettlementConfigs(success, error) {
    const url = getBaseUrl('core');

    await Axios.get(`${url}/settlement-config/default?page=1&limit=100`)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getCustomSettlementConfigs(success, error) {
    const url = getBaseUrl('core');

    await Axios.get(`${url}/settlement-config/custom?page=1&limit=100`)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async getSettlementConfigById(id, success, error) {
    const url = getBaseUrl('core');

    await Axios.get(`${url}/settlement-config/${id}/?page=1&limit=100`)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async updateSettlementConfig(id, data, success, error) {
    const url = getBaseUrl('core');

    await Axios.patch(`${url}/settlement-config/${id}`, data)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },

  async deleteSettlementConfig(id, success, error) {
    const url = getBaseUrl('core');

    await Axios.delete(`${url}/settlement-config/${id}`)
      .then(res => {
        success(res.data.data);
      })
      .catch(err => error && error(err?.response?.data));
  },
};

export default API;

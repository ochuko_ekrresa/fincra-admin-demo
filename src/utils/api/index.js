import API from './api';
import withAuthSync from './withAuthSync';

export { API, withAuthSync };
export default API;

import '@testing-library/jest-dom/extend-expect';
import 'jest-axe/extend-expect';

const Enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

const localStorageMock = {
  removeItem: jest.fn(),
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn(),
};

global.localStorage = localStorageMock;

// Configure Enzyme with React 16 adapter
Enzyme.configure({ adapter: new Adapter() });

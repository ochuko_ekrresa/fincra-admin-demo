module.exports = {
  mode: 'jit',
  purge: ['./src/pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      boxShadow: {
        header: '0px 3px 3px rgba(0, 0, 0, 0.04)',
        default: '0px 3px 0px #F2F2F2',
        'dark-100': '0px 3px 0px #1F026B',
      },
      colors: {
        black: '#000',
        current: 'currentColor',
        white: '#fff',
        'white-100': '#fafafe',
        'white-200': '#f2f2f2',
        'white-300': '#f7f7fc',
        'white-400': '#f6f5fa',
        brand: '#940EC8',
        light: '#877E9E',
        fade: '#D3D2E5',
        'fade-100': '#d3d2e54d',
        dark: '#260285',
        'dark-100': '#1F026B',
        secondary: '#4305EB',
        'secondary-100': '#3C04D1',
        'green-200': '#319780',
        'red-700': '#A34052',
      },
      fontFamily: {
        sans: [
          'Sailec',
          '-apple-system',
          'BlinkMacSystemFont',
          "'Segoe UI'",
          'Roboto',
          "'Helvetica Neue'",
          'Arial',
          "'Noto Sans'",
          'sans-serif',
          "'Apple Color Emoji'",
          "'Segoe UI Emoji'",
          "'Segoe UI Symbol'",
          "'Noto Color Emoji'",
        ],
      },
      gridTemplateColumns: {
        docs: 'repeat(auto-fit, minmax(min(20rem, 100%), 1fr))',
      },
      minHeight: {
        'full-5rem': 'calc(100vh - 5rem)',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
